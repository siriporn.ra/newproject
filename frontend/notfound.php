<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>ผลลัพธ์</title>
  <link href="https://fonts.googleapis.com/css?family=Mitr&display=swap" rel="stylesheet">
</head>
<style>
    body  {
      background: lightblue url("https://www.netpremacy.com/wp-content/uploads/2018/09/Background-website-01.jpg") no-repeat fixed center; 
      font-family: 'Mitr', serif;
      font-size: 16px;
    }

    div {
      border-radius: 5px;
      background-color: #FFA200;
      padding: 5px 30px;
      margin: 200px 500px;
      width: 320px; 
      height: 200px;
      color: white;
    }

    .column.middle {
    width: 50%;
  }
</style>
<body>
<div>
  <h1 align="center">ผลลัพธ์</h1>
  <h3 align="center">Username ของท่านไม่ถูกต้อง</h3>
  <?php
    echo "<meta  http-equiv='refresh' content='3;URL=index.php'>"
  ?>
</div>
</body>
</html>
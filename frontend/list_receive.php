<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <title>รับสินค้าและประเมินราคา</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }
        .card{
            background-color: #ffffff;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .bg{
            color:#1f0808;
        }
    </style>
</head>
<script>
      function out(){
        document.forms["fsave"].action = "receive.php";
        document.forms["fsave"].submit(); 
    }
</script>
<body>
<nav class="navbar navbar-expand-sm" style="background-color: #00b3ff;">
        <a class="navbar-brand" href="/SeniorProject/fornend/index.php" style="font-family: 'KRR_AengAei.ttf' !important; color: #32332d;font-size:35px;">
            ธวัชชัยอิเล็กทรอนิกส์
        </a>
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="navbar-nav">
        <div style="padding-left: 50px;" >
                    <button id="mdcolor" class="btn btn-warning" type="button" onclick="out()">กลับ</button>
                    </div>
</nav>

<form id="fsave" name="fsave" method="POST">
<div class="container">
    <div>
        <img src="image/unnamed2.jpg" width="1000px" height="400px" align="center"/>
    </div>
        <br><br>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist" id="uif">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#info">รายการรับและประเมินสินค้า</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#bidlist">รายการซ่อมสินค้า</a>
            </li>
        </ul>
      </form>
</body>
</html>
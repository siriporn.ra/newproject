<?php
    include "setting/config.php";
?>
<?php
    @session_start();
    @session_cache_expire(30);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>หน้าแรก</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }
        .card{
            background-color: #ffffff;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .bg{
            color:#000000  ;
        }

        .navbar{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:100vh;
    color: #fff;

    position: fixed;
}




.fo{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:30vh;
    color: #fff;
}

.form-group{
    color:#000000 ;
}


.sidenav {
  height: 72%;
  width: 15%;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #DBDDE7;
  overflow-x: hidden;
  padding-top: 20px;
}

.sidenav a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
}

.sidenav a:hover {
  color: #f1f1f1;
}



    </style>

    
<script>
    function login() {
        document.forms["flogin"].action = "login_process.php";
        document.forms["flogin"].submit();
    }
    function logoutt(){
        document.forms["logout"].action = "logout.php";
        document.forms["logout"].submit();
    }


    
</script>

    <body>
    <nav class="navbar navbar-expand-sm navbar-default fixed-top" style="width:100%; height:90px;">
    </a>
    <ul class="navbar-nav mr-auto">
    <?php

include "setting/config.php";
$strSQL="SELECT * FROM  store  ORDER BY st_name ASC";
$result=@$conn->query($strSQL);
if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
?>
<a class="navbar-brand" href="/SeniorProject/frontend/index2.php">
        <img  src="../backend/image/<?php echo $row['st_logo'] ?>"width="200px;" height="120%" style="margin-left:-50px; ">   

            <a class="nav-link active">
            <h1 style="margin-top:30px; margin-left:-50px;"><?php echo $row['st_name'] ?></h1>
                
            </a>
</a>
                <?php
    }}
                ?>


    </ul>
    <ul class="navbar-nav">
    <?php //check key
        if (@$_SESSION['key'] == md5(@$_SESSION['ow_email'])) {
            $strSQL="SELECT * FROM owners WHERE ow_email = '".$_SESSION['ow_email']."' ";
            $result = @$conn->query($strSQL);
            while($row = $result->fetch_assoc()){
        //stay in this page if have key from login page
        //เข้าสู่ระบบได้จะแสดงหน้าฟอร์มนี้

        /*<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
         <a id=\"mdcolor\" class=\"glyphicon glyphicon-log-in\"onclick=\"logoutt()\"><i class=\"fas fa-sign-out-alt\"></i> ออกจากระบบ</a>
*/
        echo " 
       
        <li class=\"nav-item\">
                     <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"><i class=\"far fa-user-circle\"></i> ผู้ใช้ระบบ : " . @$row['ow_name'] ."  สถานะ : ".$_SESSION['ow_status'].  "</a>
                     </li>
         <form id=\"logout\" name=\"logout\">
             <li class=\"nav-item\">
             <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"onclick=\"logoutt()\"cursor:pointer><i class=\"fas fa-sign-out-alt\"></i> ออกจากระบบ</a>
                
             </li>
    </form>";

            }
        } else {
        //go back to login page.php
        echo "
                    
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"cursor:pointer\"><i class=\"fas fa-sign-in-alt\"></i> เข้าสู่ระบบ</a>
                    </li>";
                   
        }
?>
        <!----><div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content" align="center">
                    <!-- Modal Header -->
                    <div class="modal-header d-block"style="background-color: #D9E2F3 ;">
                        <h2 class="form-group modal-title" style="font-weight: bold;">Log In</h2>
                    </div>
                    <form id="flogin" name="flogin" method="POST">
                    <!-- Modal body -->
                    <div class="modal-body"style="background-color: #D9E2F3  ;">
                        <img src="https://img.pngio.com/participant-png-png-image-participant-png-512_512.png" width="150px" height="150px">
                            <div class="form-group col-sm-9">
                                <label for="cus_email" style="font-weight: regular;">Username</label>
                                <input type="text" placeholder="อีเมล" class="form-control" id="ow_email" name="ow_email">
                            </div>
                            <div class="form-group col-sm-9">
                                <label for="cus_pass" style="font-weight: regular;">Password</label>
                                <input type="password" placeholder="รหัสผ่าน" class="form-control" id="ow_pass" name="ow_pass">
                            </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer d-block" align="center"style="background-color: #D9E2F3 ;">
                        <button id="mdcolor" class="btn btn-success" type="button" onclick="login()">เข้าสู่ระบบ</button>
                        <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

       
    </ul>
</nav>
    <div class="container" style="margin-top:10px; margin-left:-10px">
  <div class="row">
  <div class="col-sm-12">

</div>
</div>
<!-- ส่วน Sidebar-->
  <div class="row">

    <div class="col-sm-4" style="margin-top:100px">
      <ul class="nav nav-pills flex-column">
        <li class="nav-item">
          <a class="nav-link active" href="#">ธวัชชัยอิเล็กทรอนิกส์</a>
        </li>
        <?php 
        
        if($_SESSION['ow_status']=="เจ้าของกิจการ")
        {
            echo "
            <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"../backend/basicinformation.php\">จัดการข้อมูลพื้นฐาน</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"../backend/show_listrepair.php\">รับและประเมินราคาสินค้า</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"../backend/repair_record.php\">บันทึกการซ่อมและแจ้งรับสินค้า</a>
          </li>  
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"#\">ส่งคืนสินค้า</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"#\">ประชาสัมพันธ์</a>
          </li>  
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"#\">แจ้งปัญหาการใช้งาน</a>
          </li>";
        }
        if($_SESSION['ow_status']=="ลูกค้า")
        {
            echo "
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"#\">ประชาสัมพันธ์</a>
          </li>  
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"#\">แจ้งปัญหาการใช้งาน</a>
          </li>";
        }

        ?>
      </ul>
      <hr class="d-sm-none">
    </div>

    
    <div class="col-sm-8"  style="margin-top:100px">
      <h2>TITLE HEADING</h2>
      <h5>Title description, Dec 7, 2017</h5>
      <div class="fakeimg">Fake Image</div>
      <p>Some text..</p>
      <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
      <br>
      <h2>TITLE HEADING</h2>
      <h5>Title description, Sep 2, 2017</h5>
      <div class="fakeimg">Fake Image</div>
      <p>Some text..</p>
      <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
    </div>
  </div>
</div>


  <!--ส่วน footer-->
<div class="jumbotron text-center" style="margin-bottom:0">
<div class="row" align="center">
            <div class="col-sm-4">
                <p style="font-size: 18px; font-weight: bold; color:#000000;" href="#">เกี่ยวกับกิจการ</p>
            </div>
            <div class="col-sm-4">
                <p style="font-size: 18px; font-weight: bold; color:#000000;" href="#">ช่วยเหลือหรือติดต่อสอบถาม</p>
            </div>
            <div class="col-sm-4">
                <p style="font-size: 18px; font-weight: bold; color:#000000;" href="#">ติดตามข้อมูลข่าวสาร</p>
            </div>
        </div>
        <div class="row" align="center">
            <div class="col-sm-4">
                <a href="#" class="bg" >เกี่ยวกับเรา</a>
            </div>
            <div class="col-sm-4">
                <a href="#" class="bg">ติดต่อ</a>
            </div>
            <div class="col-sm-4">
                <a href="#"class="bg">Facebook</a>
            </div>
        </div>
        <div class="row" align="center">
            <div class="col-sm-4">
                <a href="#"class="bg">ติดต่อกิจการ</a>
            </div>
            <div class="col-sm-4">
                <a href="#" class="bg">ประชาสัมพันธ์</a>
            </div>
            <div class="col-sm-4">
                <a href="#" class="bg"></a>
            </div>
        </div>
        <div class="row" align="center">
            <div class="col-sm-4">
                <a href="#"class="bg">เงื่อนไขการใช้บริการ</a>
            </div>
            <div class="col-sm-4">
                <a href="#" class="bg">การลงทะเบียน</a>
            </div>
            <div class="col-sm-4">
                <a href="#" class="bg"></a>
            </div>
        </div>
        <div class="row" align="center">
            <div class="col-sm-4">
                <a href="#"class="bg"></a>
            </div>
            <div class="col-sm-4">
                <a href="#" class="bg"></a>
            </div>
            <div class="col-sm-4"></div>
        </div>
        <br>
                    <div>
                        <label style="color: #000000;font-size: 14px;">&copy;&nbsp;2020 ThawatchaiElectric.com</label>
                        <a data-toggle="modal" data-target="#adminlogin" style="cursor:pointer">&copy;</a><label style="color: #000000;font-size: 14px;">&copy;&nbsp;2020 ThawatchaiElectric.com</label>
                    </div>
</div>

    </body>
</head>


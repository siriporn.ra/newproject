<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>บันทึกรายการซ่อม</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }
        .card{
            background-color: #ffffff;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .bg{
            color:#1f0808;
        }
    </style>
</head>

        <script>
            
    function save(){
        document.forms["fsave"].action = "register_process.php";
        document.forms["fsave"].submit(); 
    }

    function out(){
        document.forms["fsave"].action = "index.php";
        document.forms["fsave"].submit(); 
    }
        </script>
<body>
<nav class="navbar navbar-expand-sm" style="background-color: #00b3ff;">
        <a class="navbar-brand" href="/SeniorProject/fornend/index.php" style="font-family: 'KRR_AengAei.ttf' !important; color: #32332d;font-size:35px;">
        <img src="image/logo1.png" width="80px;" />ธวัชชัยอิเล็กทรอนิกส์
        </a>
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="navbar-nav">
        <div style="padding-left: 50px;" >
                    <button id="mdcolor" class="btn btn-warning" type="button" onclick="out()">กลับ</button>
                    </div>
</nav>

     <!-- Modal Header -->
     <div class="modal-header d-block">
                    <h2 class="modal-title" style="font-weight: bold;"align="center">บันทึกรายการซ่อม</h2>
                   
                </div>
                <form id="fsave" name="fsave" method="POST">
                <!-- Modal body -->
                <div class="modal-body" style="font-size:16px;">
                    <center><img src="./icons/registerlogo.png" width="150" height="150" /></center><br><br>
                        <div class="form-group">
                            <div class="row">
                                <div style="padding-left: 40px;" class="col-sm-4">
                                    <label style="font-weight: regular;">รหัสซ่อมสินค้า : </label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="rec_id" name="rec_id">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div style="padding-left: 40px;" class="col-sm-4">
                                    <label style="font-weight: regular;">สถานะการซ่อม : </label>
                                </div>
                                <div class="col-sm-6">
                                        <select name="rec_status" id="rec_status" style="width:30%;text-align-right: right;">
                                            <option value="กำลังดำเนินการซ่อม">กำลังดำเนินการซ่อม</option>
                                            <option value="ซ่อมเสร็จแล้ว">ซ่อมเสร็จแล้ว</option>
                                            <option value="แจ้งรับสินค้า">แจ้งรับสินค้า</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div style="padding-left: 40px;" class="col-sm-4">
                                    <label style="font-weight: regular;">วันที่บันทึกสินค้า: </label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="date" style="width: 300px;" class="form-control" id="rec_date" name="rec_date"  min="1900-01-01" max="2002-12-31"/>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div style="padding-left: 40px;" class="col-sm-4">
                                    <label style="font-weight: regular;">รายละเอียดการซ่อม : </label>
                                </div>
                                <div class="col-sm-6">
                                    <textarea type="text"  class="form-control" rows="4" id="rec_price" name="rec_price"></textarea>
                                </div>
                        </div>
                <!-- Modal footer -->
                <div class="modal-footer d-block" align="center">
                    <button id="mdcolor" class="btn btn-success" type="button" onclick="save()">บันทึก</button>
                    <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
                </div>
                </form>
            </div>
        </div>
    </div>

</body>
</html>
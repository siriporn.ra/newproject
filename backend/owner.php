<?php

include "setting/config.php";


@session_start();
$alert = @$_SESSION['success'];
unset($_SESSION["success"]);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>จัดการข้อมูลพื้นฐาน</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }
        
        .card{
            background-color: #ffffff;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .bg{
            color:#000000  ;
        }

        .navbar{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
   /* height:100vh;*/
    color: #fff;


    width:100%; 
    height:90px;

}




.fo{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:30vh;
    color: #fff;
}

.form-group{
    color:#000000 ;
}


.sidenav {
  height: 72%;
  width: 15%;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #DBDDE7;
  overflow-x: hidden;
  padding-top: 20px;
}

.sidenav a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
}

.sidenav a:hover {
  color: #f1f1f1;
}

    </style>
</head>
<script>
    
function out(){
        document.forms["fsave"].action = "basicinformation.php";
        document.forms["fsave"].submit(); 
    }
    function login() {
        document.forms["flogin"].action = "login_process.php";
        document.forms["flogin"].submit();
    }
function logoutt(){
        document.forms["logout"].action = "/SeniorProject/fornend/index.php";
        document.forms["logout"].submit();
    }

</script>
<body  style="background-color: #FFFFFF ;">



<!-- Page Content -->
<!-- Sidebar -->

  <nav class="navbar navbar-expand-sm"  >
        <ul class="navbar-nav mr-auto">
        <?php

include "setting/config.php";
$strSQL="SELECT * FROM  store  ORDER BY st_name ASC";
$result=@$conn->query($strSQL);
if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
?>
<a class="navbar-brand" href="/SeniorProject/frontend/index2.php">
        <img  src="../backend/image/<?php echo $row['st_logo'] ?>"width="200px;" height="120%" style="margin-left:-50px; ">   
            <a class="nav-link active">
            <h1 style="margin-top:30px; margin-left:-50px;"><?php echo $row['st_name'] ?></h1>
                
            </a>
</a>
                <?php
    }}
                ?>
        </ul>
        <ul class="navbar-nav ">
        <?php //check key
        $strSQL="SELECT * FROM owners";
        $result = @$conn->query($strSQL);
            if (@$_SESSION['key'] == md5(@$_SESSION['ow_email'])) {
                $strSQL="SELECT * FROM owners WHERE ow_email = '".$_SESSION['ow_email']."' ";
                $result = @$conn->query($strSQL);
                while($row = $result->fetch_assoc()){
            //stay in this page if have key from login page
            //เข้าสู่ระบบได้จะแสดงหน้าฟอร์มนี้

            echo " 
           
            <li class=\"nav-item\">
            <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"><i class=\"far fa-user-circle\"></i> ผู้ใช้ระบบ : " . @$row['ow_name'] ."  สถานะ : ".$_SESSION['ow_status'].  "</a>
            </li>
             <form id=\"logout\" name=\"logout\">
                 <li class=\"nav-item\">
                 <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"onclick=\"logoutt()\"cursor:pointer><i class=\"fas fa-sign-out-alt\"></i> ออกจากระบบ</a>
                    
                 </li>
        </form>";
                }
            } else {
            //go back to login page.php
            echo "
                        
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#myModaldlogin\" style=\"cursor:pointer\"><i class=\"fas fa-sign-in-alt\"></i> เข้าสู่ระบบ</a>
                        </li>";
                       
            }
    ?>
            <!----><div class="modal " id="myModaldlogin">
                <div class="modal-dialog">
                    <div class="modal-content" align="center">
                        <!-- Modal Header -->
                        <div class="modal-header d-block"style="background-color: #D9E2F3 ;">
                            <h2 class="form-group modal-title" style="font-weight: bold;">Log In</h2>
                        </div>
                        <form id="flogin" name="flogin" method="POST">
                        <!-- Modal body -->
                        <div class="modal-body"style="background-color: #D9E2F3  ;">
                            <img src="https://img.pngio.com/participant-png-png-image-participant-png-512_512.png" width="150px" height="150px">
                                <div class="form-group col-sm-9">
                                    <label for="cus_email" style="font-weight: regular;">Username</label>
                                    <input type="text" placeholder="อีเมล" class="form-control" id="ow_email" name="ow_email">
                                </div>
                                <div class="form-group col-sm-9">
                                    <label for="cus_pass" style="font-weight: regular;">Password</label>
                                    <input type="password" placeholder="รหัสผ่าน" class="form-control" id="ow_pass" name="ow_pass">
                                </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center"style="background-color: #D9E2F3 ;">
                            <button id="mdcolor" class="btn btn-success" type="button" onclick="login()">เข้าสู่ระบบ</button>
                            <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

           
        </ul>
    </nav>


    

    <div class="container">
    <div class="row">
<div class="col-md-2" style="margin-top:50px; padding-left:-50px;">
  <ul class="nav nav-pills flex-column " style="margin-left:-50px;">
    <li class="nav-item" >
                    <?php

                include "setting/config.php";

                $strSQL="SELECT * FROM  store  ORDER BY st_name ASC";

                $result=@$conn->query($strSQL);

                if($result->num_rows>0){
                    while($row=$result->fetch_assoc()){
                ?>
                            <a class="nav-link active" href="#">
                                    <h6><?php echo $row['st_name'] ?></h6>
                                  
                            </a>
                                <?php
                    }}
                                ?>
              
    </li>
    <?php 
    
    if($_SESSION['ow_status']=="เจ้าของกิจการ")
    {
        echo "
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"owner.php\" >ข้อมูลส่วนตัว</a>
        </li>
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../backend/basicinformation.php\">จัดการข้อมูลพื้นฐาน</a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../backend/assess1.php\">รับและประเมินราคาสินค้า</a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">ซ่อมและแจ้งรับสินค้า</a>
      </li>  
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">ส่งคืนสินค้า</a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">ประชาสัมพันธ์</a>
      </li>  
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">แจ้งปัญหาการใช้งาน</a>
      </li>";
    }
    if($_SESSION['ow_status']=="ลูกค้า")
    {
        echo "
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">ประชาสัมพันธ์</a>
      </li>  
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">แจ้งปัญหาการใช้งาน</a>
      </li>";
    }

    ?>
  </ul>
  <hr class="d-sm-none">
</div>

<div class="col-md-8 col-sm-4" style="background-color: #C4D8F1 ; margin-left:50px;" >

                     
                <h1 align="center" class="font" style="padding-top: 20px; font-size:40px">ข้อมูลผู้ใช้ระบบ</h1>

              
<br>


<div class="container"style="margin-right:-50px;">      

<?php
echo $alert;
?>

<div id="info" class="container tab-pane active"><br><br>
                    <h3>เจ้าของกิจการ </h3>

                    <table class="table table-bordered font col-sm-12">
   
                                <tr class="t col-sm-12" align="center" style="font-size:18px;">
                                    <th >ชื่อ-นามสกุล</th>
                                    <th  >ที่อยู่</th>
                                    <th  >เบอร์โทรศัพท์</th>
                                    <th  >E-mail</th>
                                    <th  >สถานะ</th>
                                    <th  >รหัสผ่าน</th>
                                    <th  width="10%">
                                
                                </tr> 

        <?php
          $strSQL="SELECT * FROM owners WHERE ow_status = 'เจ้าของกิจการ' AND ow_id = '".$_SESSION['ow_id']."' ORDER BY ow_id ASC" ;
          $result=@$conn->query($strSQL);
        ?>

<?php
  if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
?>

                    <tr style="font-size:12px;" class="font" >
                        <td><?php echo $row['ow_name']; ?></td> 
                        <td><?php echo $row['ow_add']; ?></td> 
                        <td><?php echo $row['ow_tel']; ?></td> 
                        <td><?php echo $row['ow_email']; ?></td> 
                        <td><?php echo $row['ow_status']; ?></td> 
                        <td>************</td> 
                        <td align="center">
                    <a data-toggle="modal" data-target="#edit" 
                         utel="<?php echo $row['ow_tel'];  ?>"
                         uname="<?php echo $row['ow_name'];  ?>"
                         uadd="<?php echo $row['ow_add'];?>"
                         uemail="<?php echo $row['ow_email'];  ?>"
                         ustatus="<?php echo $row['ow_status'];  ?>"
                         upass="<?php echo $row['ow_pass'];  ?>"
                         uservice="<?php echo $row['ow_service_rates'];  ?>"
                         onclick="edituser(this)" style="cursor:pointer;"><button style="background-color: #5FF891">แก้ไข</button></a>
                                          
                      </td>
                </tr>
      <?php
      
    }}
    ?>
      </table>

<!--แก้ไขข้อมูล-->
<div class="modal" id="edit" >
    <div class="modal-dialog modal-lg" style="border:4px #FF6D33  solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header d-block">
          <h2 class="modal-title font" style="font-weight: bold;" align="center">แก้ไขข้อมูลผู้ใช้งาน</h2>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body font">
        <center><img src="image/edit-valid.png" wigth="200px" height="200px" ></center>
        <br>
        <form id="formupdateuser" name="formupdateuser" method="POST">
              <label class="font" style="font-size: 20px;">ชื่อ:</label>
              <input type="text" class="form-control" id="uname" name="uname" placeholder="ชื่อ">
            <br>
              <label class="font" style="font-size: 20px;">เบอร์โทรศัพท์:</label>
              <input  type="text" class="form-control" id="utel" name="utel" placeholder="เบอร์โทรศัพท์" maxlength="10" minlength="10">
              <input   type="text" class="form-control" id="utelold" name="utelold" hidden maxlength="10" minlength="10">
            <br>
              <label class="font" style="font-size: 20px;">สถานะ:</label>
              <select   class="form-control"  id="ustatus" name="ustatus"  >
                <option  value="เจ้าของกิจการ">เจ้าของกิจการ</option>
              </select>                                        <br>
              <label class="font" style="font-size: 20px;">ที่อยู่:</label>
              <textarea type="text" class="form-control" id="uadd" name="uadd" placeholder="ที่อยู่"></textarea>
            <br>
              <label class="font" style="font-size: 20px;">อีเมล:</label>
              <input type="text" class="form-control" id="uemail" name="uemail" placeholder="อีเมล"></input>
            <br>
              <label class="font" style="font-size: 20px;">รหัสผ่าน:</label>
              <input type="password" class="form-control" id="upass" name="upass" placeholder="รหัสผ่าน"></input>
            <br>
                                     
                                      <!-- Modal footer -->
        <div class="modal-footer d-block" align="center"  style="background-color: #E2FA6E;">
          <button id="mdcolor" class="btn btn-success" type="button" data-dismiss="modal" onclick="acceptupdate()">ยืนยันการแก้ไข</button>
          <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
        </div>
      </div>
    </div>
  </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />



<script>

function acceptupdate(){
document.forms["formupdateuser"].action="updateowners.php";
document.forms["formupdateuser"].submit();

  }

function edituser(obj){
 
    var ownname =obj.getAttribute("uname");
    var owntel =obj.getAttribute("utel");
    var ownadd =obj.getAttribute("uadd");
    var ownemail =obj.getAttribute("uemail");
    var ownpass =obj.getAttribute("upass");

    document.getElementById("uname").value = ownname;
    document.getElementById("utel").value = owntel;
    document.getElementById("utelold").value = owntel;
    document.getElementById("uadd").value = ownadd;
    document.getElementById("uemail").value = ownemail;
    document.getElementById("upass").value = ownpass;

}






/*<!-- ลบข้อมูล -->*/

var globalid;
function sdeletedata(obj){
   
    var protid =obj.getAttribute("delete_id");
    var protname =obj.getAttribute("delete_name");
    globalid=protid

    document.getElementById("deletename").innerHTML = protname;
    document.getElementById("deleteid").innerHTML = protid;
}

function deletedata(){
  //  alert(globalid)
//alert(globalid);
    $.ajax({ //กล่องๆนึงรวมข้อมูลไว้
                type:"POST",
                async :false, //ทำให้ทำงานเรียงตามลำดับ
                url:"webservice/deleteowner.php",
                data:{
                    delete_id:globalid
                }
            });
            location.reload();
}
</script>





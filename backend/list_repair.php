<?php
    include "setting/config.php";
    @session_start();
    @session_cache_expire(30);

    $rec_idfromedit = $_GET["formedit"];
    $datereveives = "";
    $recstatus = "";
    $ow_data  = "";
    $stredit_list="SELECT * FROM  receives 
    INNER JOIN owners ON receives.ow_id = owners.ow_id
    WHERE rec_id='".$rec_idfromedit."' ";
    $resultstredit_list=@$conn->query($stredit_list);
    if($resultstredit_list->num_rows>0){
        while($rowstredit_list=$resultstredit_list->fetch_assoc()){
            $ow_data=$rowstredit_list['ow_name'].":".$rowstredit_list['ow_id'];
            $datereveives = $rowstredit_list['rec_date'];
            $recstatus = $rowstredit_list['rec_status'];
        }}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>แก้ไขบิลรายการรับและประเมินราคา</title>
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }
        .card{
            background-color: #ffffff;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .bg{
            color:#000000  ;
        }

        .navbar{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:100vh;
    color: #fff;

    position: fixed;
}




.fo{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:30vh;
    color: #fff;
}

.form-group{
    color:#000000 ;
}

body{
        background:#FFFFFF;
    }

/***************************************************************** */
    </style>

    
<script>

    function login() {
        document.forms["flogin"].action = "login_process.php";
        document.forms["flogin"].submit();
    }
    function logoutt(){
        document.forms["logout"].action = "logout.php";
        document.forms["logout"].submit();
    }
    function add_row() {
        var table = document.getElementById("myTable");
        count_rows = table.getElementsByTagName("tr").length;

        var row = table.insertRow(count_rows);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);

        cell1.innerHTML = "<input type='text' name='txtA'"+count_rows+"value>";
    }

    function btadd_owner() {
        document.forms["add_owner"].action = "add_owners.php";
        document.forms["add_owner"].submit();
    }
    function del_row(){
        var table = document.getElementById("myTable");
        count_rows = table.getElementsByTagName("tr").length;
        document.getElementById("myTable").deleteRow(count_rows-1);
    }
   
    
</script>

    <body class="container col-lg-10" align="center" style="background-color:#F0FFFF;">

    <div  style="margin-top:15px;">
  <div class="row">
  <div class="col-md-12">
<nav class="navbar navbar-expand-sm navbar-default fixed-top" style="width:100%; height:90px;">
    </a>
    <ul class="navbar-nav mr-auto">
    <?php


$strSQL="SELECT * FROM  store  ORDER BY st_name ASC";
$result=@$conn->query($strSQL);
if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
?>
<a class="navbar-brand" href="/SeniorProject/frontend/index2.php">
        <img  src="../backend/image/<?php echo $row['st_logo'] ?>"width="200px;" height="120%" style="margin-left:-50px; ">   

            <a class="nav-link active">
            <h1 style="margin-top:30px; margin-left:-50px;"><?php echo $row['st_name'] ?></h1>
                
            </a>
</a>
                <?php
    }}
                ?>


    </ul>
    <ul class="navbar-nav">
    <?php //check key
        if (@$_SESSION['key'] == md5(@$_SESSION['ow_email'])) {
            $strSQL="SELECT * FROM owners WHERE ow_email = '".$_SESSION['ow_email']."' ";
            $result = @$conn->query($strSQL);
            while($row = $result->fetch_assoc()){
        //stay in this page if have key from login page
        //เข้าสู่ระบบได้จะแสดงหน้าฟอร์มนี้

        echo " 
       
        <li class=\"nav-item\">
                     <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"><i class=\"far fa-user-circle\"></i> ผู้ใช้ระบบ : " . @$row['ow_name'] ."  สถานะ : ".$_SESSION['ow_status'].  "</a>
                     </li>
         <form id=\"logout\" name=\"logout\">
             <li class=\"nav-item\">
             <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"onclick=\"logoutt()\"cursor:pointer><i class=\"fas fa-sign-out-alt\"></i> ออกจากระบบ</a>
                
             </li>
    </form>";

            }
        } else {
        //go back to login page.php
        echo "
                    
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"cursor:pointer\"><i class=\"fas fa-sign-in-alt\"></i> เข้าสู่ระบบ</a>
                    </li>";
                   
        }
?>
        <!----><div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content" align="center">
                    <!-- Modal Header -->
                    <div class="modal-header d-block"style="background-color: #D9E2F3 ;">
                        <h2 class="form-group modal-title" style="font-weight: bold;">Log In</h2>
                    </div>
                    <form id="flogin" name="flogin" method="POST">
                    <!-- Modal body -->
                    <div class="modal-body"style="background-color: #D9E2F3  ;">
                        <img src="https://img.pngio.com/participant-png-png-image-participant-png-512_512.png" width="150px" height="150px">
                            <div class="form-group col-sm-9">
                                <label for="cus_email" style="font-weight: regular;">Username</label>
                                <input type="text" placeholder="อีเมล" class="form-control" id="ow_email" name="ow_email">
                            </div>
                            <div class="form-group col-sm-9">
                                <label for="cus_pass" style="font-weight: regular;">Password</label>
                                <input type="password" placeholder="รหัสผ่าน" class="form-control" id="ow_pass" name="ow_pass">
                            </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer d-block" align="center"style="background-color: #D9E2F3 ;">
                        <button id="mdcolor" class="btn btn-success" type="button" onclick="login()">เข้าสู่ระบบ</button>
                        <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

       
    </ul>
</nav>
</div>
</div>

<div  style="margin-left:100px;">
<!--logoส่วนที่ 2 -->
<br><br><br><br>
<h3  align="center">บันทึกรายการซ่อมและแจ้งรับสินค้า</h3>
    <div class="row">
        <div class="col-sm-4">  
                          
        </div>
    </div>


<div  align="right" style="margin-top:-30px; ">
    <p style="font-size:16px; margin-right:-80px; " id="num_quo" name="num_quo">เลขที่ใบเสนอราคา :
                                <?php
                       
                         echo $rec_idfromedit;
                                          
                            ?>
                                
                                </p>

    <p style="margin-right:-80px;" id="date" name="date">วันที่รับ :
    
    <?php
            echo $datereveives;
        ?>
     </p>

     

    <span style="margin-right:-80px;">สถานะ : 
    <select   id="ptypeid" name="ptypeid"
    style="font-size:16px;width:130px;" onchange="updaterecstatus()">
            <?php if($recstatus=="0") {?>
               <option hidden value="0">รับสินค้า</option>
              
            <?php }?>
            <?php if($recstatus=="1") {?>
                <option hidden value="1">แจ้งรับ</option>
           
            <?php }?>
            <?php if($recstatus=="2") {?>
                <option hidden value="2">ส่งคืน</option>
            
            <?php }?>
        
          

            </select>
    </span>
    <br><br>
    <script>
    function updaterecstatus(){
        var rec_status = document.getElementById("ptypeid").value;
        var rec_id = "<?php echo $rec_idfromedit;?>";
       // console.log(rec_status+rec_id);
                $.ajax({

                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/edit_repair_status.php",
                data:JSON.stringify({
                    //eq_price:eq_price,
                    rec_status:rec_status,
                    rec_id:rec_id
                }),
                success: function (response) {
                var json_data = response.result;
                if(json_data=="Success"){
                alert("แก้ไขสถานะสำเร็จ");
                    location.reload();
                }
                }
                });
    }
    </script>
  



</div>
<script>


        function insertdetailreceives(){
           //ข้อมูลจาก Input box ชื่อลูกค้า
           // รหัสประเภท
           //
            var proname= document.getElementById("pro_name").value;
            var dec= document.getElementById("list_dec").value;
            var num= document.getElementById("number").value;
            var list_rec_price= document.getElementById("list_rec_price").value;
           

           $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/getlist_data.php",
                data:JSON.stringify({
                    proname:proname,
                    dec:dec,
                    num:num,
                    list_rec_price:list_rec_price
                }),
                success: function (response) {
                var json_data = response;
               //console.log(response.result);
               if(response.result=="Success"){
                location.reload(); 
               }

                }
            });

//เช็คค่าว่าง

    
        }
//เลือกสินค้าและประเภทสินค้า
        function getpro_type(){
            var pro_type = document.getElementById("eq_typeid").value;
 //console.log(pro_type);
        
 $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/getproduct_type.php",
                data:JSON.stringify({
                    pro_type:pro_type
                }),
                success: function (response) {
                var json_data = response;
                var product =" <option hidden>กรุณาเลือกข้อมูล</option>"
               
                $.each(response, function(index) {
                       // console.log(response[index].pro_id);
                       // console.log(response[index].pro_name);
                        product += "<option value="+"'"+response[index].pro_id+"'"+">"+response[index].pro_name+"</option>";
                    });
                    $('#pro_name').html(product);
                }
            });
 
        }
//เลือกอุปกรณ์ที่ใช้
        function add_rec(obj) {
            
            var list_rec_id = obj.getAttribute("list_rec_idofequ");
            var eq_name = document.getElementById("equ"+list_rec_id).value;
           //console.log(eq_name+list_rec_id);

            
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/get_list_receives.php",
                data:JSON.stringify({
                    eq_name:eq_name,
                    list_rec_id:list_rec_id
                }),
                success: function (response) {
                var json_data = response.result;
                    if(json_data=="Success"){
                        location.reload();
                    }
                }
            });
    
         
        }
    
       


</script>


<script>
function getproductname(){
   // console.log("proname");
    var proname = document.getElementById("pro_name").value;
   // console.log(proname);
    
}
</script>

<div class="row">

<div  align="left"> 
<a> 
  <label for="browser"> ชื่อลูกค้า :</label>
  <input  name="nameandid" id="nameandid" onchange="getoutdata(this)" value="<?php echo $ow_data; ?>" readonly>

<script>
function checktextbox(){
    if(document.getElementById("nameandid").value==""){

        document.getElementById("customer_tel").value="";
        document.getElementById("customer_address").value="";
    }
}
setInterval(function(){ 
    getoutdata()
    checktextbox()
}, 100);

function getoutdata(){
    var customer_name_and_id= document.getElementById("nameandid").value;
 //   console.log(customer_name_and_id);
    
    var customer_id = customer_name_and_id.split(":")[1];
 //   console.log(customer_id);

        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/getcustomerdetail.php",
                data:JSON.stringify({
                    customer_id:customer_id
                }),
                success: function (response) {
                var json_data = response;
     //           console.log(json_data);

document.getElementById("customer_tel").value=json_data.ow_tel;
document.getElementById("customer_address").value=json_data.ow_add;
                }
            });

}
</script>




<a>ที่อยู่ : <input type="text" id="customer_address" Disabled></a>
<a>เบอร์โทรศัพท์ : <input type="text" id="customer_tel" Disabled></a>

 <form id="add_owner" name="add_owner" method="POST" style="margin-left:800px; margin-top:-35px;">
    <button onclick="btadd_owner()"style="background-color: #8eceff;"disabled>เพิ่มลูกค้าใหม่</button>
</form>
<br>
   
</div>


</div>

<br>
<table class="table table-info "  >
   <tr  class="table table-primary "align="center" style="font-size:18px;">
        <th >ลำดับ</th>
        <th  >สินค้า</th>
        <th  >อาการที่เสีย</th>
        <th  >จำนวน</th>
        <th  >ราคาประเมินรวม</th>
        <!--<th  >สถานะการซ่อม</th>-->
        <th>รายละเอียดการซ่อม</th>
        <th  >จัดการข้อมูล</th>
   </tr>

                                 
      <?php
$strSQL="SELECT * FROM receive_list 
INNER JOIN product ON receive_list.pro_id = product.pro_id 
INNER JOIN product_type ON product.pro_type_id  = product_type.pro_type_id WHERE rec_id='".$rec_idfromedit."' ";
$result=@$conn->query($strSQL);
$totalprice=0;
   

      ?>
<?php

if($result->num_rows>0){


    while($row=$result->fetch_assoc()){
        $totalprice = $totalprice + $row['list_rec_price'];
      ?>
                    <tr style="font-size:16px;" class="font" align="center">
                        <td><?php echo $row['list_rec_id']; ?></td> 
                        <td><?php echo $row['pro_name']; ?></td> 
                        <td><?php echo $row['list_rec_decline']; ?></td>
                        <td><?php echo $row['list_rec_num']; ?></td>
                        <td align="right"><?php echo $row['list_rec_price']; ?></td>   
                      <td><input type=text id="list_rec_detail<?php echo $row['list_rec_id'];  ?>" name="list_rec_detail" value="<?php echo $row['list_rec_edit_status']; ?>" disabled></td>




                        
                        <td>
                   <!-- <?php
                        if($row["list_rec_edit_status"]==""){ ?>
        <input type="checkbox" list_rec_id="<?php echo $row["list_rec_id"]?>" onclick="list_edit_status(this)"  class=form-control >
                        <?php

                    }else{

                          
                        ?>
         <input type="checkbox" list_rec_id="<?php echo $row["list_rec_id"]?>"onclick="list_edit_status(this)"  class=form-control checked>
                    <?php 
                    }
                    ?>-->
                    
                    <script>
                        function list_edit_status(obj){
                            var list_rec_id = obj.getAttribute("list_rec_id");

                            $.ajax({
                            type: "POST",
                            dataType: 'json',
                            contentType: 'application/json',
                            async : false,
                            url: "webservice/edit_repair_status.php",
                            data:JSON.stringify({
                                //eq_price:eq_price,
                                list_rec_id:list_rec_id
                        
                                
                            }),
                            success: function (response) {
                            var json_data = response.result;
                            if(json_data=="Success"){
                                location.reload();
                            }
                            }
                            });
                        }

                    </script>
                   
                    </td>   
                       

                        <td>
                        <a 
                        list_rec_id ="<?php echo $row['list_rec_id'];  ?>"
                        onclick="get_receive_list_detail(this) " style="cursor:pointer;" >
                        <img src="image/edit_new.png" wigth="35px" height="35px"></a>
                    
                     
<script>
//ลบข้อมูลรายการรับ
var globalid;
function deletedata_receive_list(obj){
   
    var protid =obj.getAttribute("delete_id");
    var protname =obj.getAttribute("delete_name");
    globalid=protid

    document.getElementById("deletename").innerHTML = protname;
    document.getElementById("deleteid").innerHTML = protid;
}

            function deletedata_receive_list_detail(obj) {
                $.ajax({ //กล่องๆนึงรวมข้อมูลไว้
                type:"POST",
                async :false, //ทำให้ทำงานเรียงตามลำดับ
                url:"webservice/delete_list_recevice.php",
                data:{
                    delete_id:globalid
                }
            });
            location.reload();
}
          

</script>

       



         <script>
            function get_receive_list_detail(obj) { 
              

                var list_rec_id = obj.getAttribute("list_rec_id");
                var textlist_rec_id = "list_rec_detail"+list_rec_id;
               
                var status=  document.getElementById(textlist_rec_id).disabled;
                if(status==true){
                    document.getElementById(textlist_rec_id).disabled =false;     
                }else{
                    var datatext = document.getElementById(textlist_rec_id).value;
                    console.log(datatext);          
                $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/edit_repair_status.php",
                data:JSON.stringify({
                    list_rec_id:list_rec_id,
                    datatext:datatext
                }),
                success: function (response) {
                var json_data = response;
                location.reload();
             
              
                }
            });



      }


                
            }
         
            function acceptupdate_list_recevice(){
                document.forms["update_list_recvice"].action="update_list_recevice.php";
                document.forms["update_list_recvice"].submit();

            }

      
            

         </script>

         <script>
    function get_list_proname(){

    var producttype1 = document.getElementById("protype_id").value;

    $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/getproduct_type.php",
                data:JSON.stringify({
                    pro_type:producttype1
                }),
                success: function (response) {
                var json_data = response;
                var product =" <option hidden>กรุณาเลือกข้อมูล</option>"
               
                $.each(response, function(index) {
                        console.log(response[index].pro_id);
                        console.log(response[index].pro_name);
                        product += "<option value="+"'"+response[index].pro_id+"'"+">"+response[index].pro_name+"</option>";
                    });
                    $('#product1').html(product);
                }
            });

}
    </script>
                      
                       <!--แก้ไขข้อมูล-->
      
                </tr>
      <?php
      
}}
?>

</table>

        <p  align="right">ราคารวมรายการสินค้า : <span id="list_total" name="list_total"><?php echo $totalprice; ?></span>  บาท</p>
  




<!--คำนวนราคา-->

<div align="right"> 

                    <?php
              
                $strSQL="SELECT * FROM  store  ORDER BY st_service_rates ASC";
                $result=@$conn->query($strSQL);
                if($result->num_rows>0){
                    while($row=$result->fetch_assoc()){
                ?>
                    <a>
                        <p >อัตราค่าบริการ : 
                        <span id="st_service_rates" name="st_service_rates">
                        <?php echo $row['st_service_rates'] ?> 
                        </span>
                        บาท   
                        </p>
                                 
                    </a>
                                <?php
                    }}
                                ?>

                   
                     <script>

                     function sumtotal(){
                      

                         var list_rec_price=document.getElementById("list_rec_price").value;
                         var list_rec_price=parseInt(list_rec_price);
                         var st_service_rates=document.getElementById("st_service_rates").innerHTML;
                         var st_service_rates = parseInt(st_service_rates.trim());
                         document.getElementById("total").innerHTML=st_service_rates+list_rec_price;

                     }
                     </script>
                     
                    <p>รวมเป็นเงิน :<span id="sumtotal" name="sumtotal">
                    </span> บาท 
                    <button id="save_list_receives" name="save_list_receives" onclick="save_list_rec()"style="background-color: #66CDAA	;"disabled>บันทึก</button>

                        
                    <?php
              
              $strSQL="SELECT * FROM  assign WHERE rec_id='".$rec_idfromedit."' ";
              $result=@$conn->query($strSQL);
              if($result->num_rows>0){
                  while($row=$result->fetch_assoc()){
              ?>
                                      <p>***สามารถรับสินค้าได้ในวันที่ :   <?php echo $row['ass_date'] ?> </p>

                              <?php
                  }}
                              ?>
         
</div>
<form id="items" name="items" method="POST">
<p align="center" data-toggle="tooltip" data-placement="top" title="#">
<!-- <button class="button button2" style="cursor:pointer" onclick="location.href='show_list_rec.php?id=<?php echo $row['rec_id'] ?>'">รายการรับ</button> -->
<button type="button" class="btn btn-info"   onclick="all_items()">ยืนยันรายการซ่อม</button></p>
</form>


<script>
 function all_items(){
     var rec_id = "<?php echo $rec_idfromedit; ?>";
     console.log(rec_id);

                $.ajax({
                            type: "POST",
                            dataType: 'json',
                            contentType: 'application/json',
                            async : false,
                            url: "webservice/accept_list_repair.php",
                            data:JSON.stringify({
                                //eq_price:eq_price,
                                rec_id:rec_id
                        
                                
                            }),
                            success: function (response) {
                            var json_data = response.result;
                            if(json_data=="Success"){
                                alert("ยืนยันการซ่อมสำเร็จ");
                                location.reload();
                            }else{
                                alert("คุณยังซ่อมไม่หมด ไม่สามารถยืนยันการซ่อมได้");
                               
                            }
                            }
                            });
    }
setInterval(function(){
    
     var list_total = parseFloat(document.getElementById("list_total").innerHTML.trim());
   
     var service_price = parseFloat(document.getElementById("st_service_rates").innerHTML.trim());
    
     var total=list_total+service_price;
    
     document.getElementById("sumtotal").innerHTML=total;

}, 1000);

     function save_list_rec() {
      //  var rec_id  = document.getElementById("num_quo").value;
      //  var date = document.getElementById("date").value;
      //  var rec_status = document.getElementById("ptypeid").value;
        var ow_id  = document.getElementById("nameandid").value;
        //var eq_price = parseFloat( document.getElementById("total").innerHTML.trim());
        var ass_date  = document.getElementById("rec_date").value;
       // console.log(eq_price);
        var ow_id = ow_id.split(":")[1];
       
       
       $.ajax({

                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/save_list.php",
                data:JSON.stringify({
                    //eq_price:eq_price,
                    ow_id:ow_id,
                    ass_date:ass_date
                    
                }),
                success: function (response) {
                var json_data = response.result;
                if(json_data=="Success"){
                   alert("บันทึกสำเร็จ");
                    location.reload();
                }
                }
            });
   
}
        
       
</script>
    </body>
</head>
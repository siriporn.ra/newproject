<?php

include "setting/config.php";


@session_start();
$alert = @$_SESSION['success'];
unset($_SESSION["success"]);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>จัดการข้อมูลพื้นฐาน</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }
        
        .card{
            background-color: #ffffff;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .bg{
            color:#000000  ;
        }

        .navbar{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
   /* height:100vh;*/
    color: #fff;


    width:100%; 
    height:90px;

}




.fo{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:30vh;
    color: #fff;
}

.form-group{
    color:#000000 ;
}


.sidenav {
  height: 72%;
  width: 15%;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #DBDDE7;
  overflow-x: hidden;
  padding-top: 20px;
}

.sidenav a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
}

.sidenav a:hover {
  color: #f1f1f1;
}

    </style>

</head>
<script>
     function w3_open() {
  document.getElementById("mySidebar").style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}



function adddata(){
   
document.forms["form1"].action="storeprocess.php";
document.forms["form1"].submit();  
}


function login() {
        document.forms["flogin"].action = "login_process.php";
        document.forms["flogin"].submit();
    }
    function logoutt(){
        document.forms["logout"].action = "/SeniorProject/frontend/index.php";
        document.forms["logout"].submit();
    }


</script>
<body  style="background-color: #FFFFFF;">

<!-- Page Content -->


  <!--เพิ่มข้อมูลผู้ใช้งาน-->
  
  <nav class="navbar navbar-expand-sm"  >
        <ul class="navbar-nav mr-auto">
        <?php

include "setting/config.php";
$strSQL="SELECT * FROM  store  ORDER BY st_name ASC";
$result=@$conn->query($strSQL);
if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
?>
<a class="navbar-brand" href="/SeniorProject/frontend/index2.php">
        <img  src="../backend/image/<?php echo $row['st_logo'] ?>"width="200px;" height="120%" style="margin-left:-50px; ">   

            <a class="nav-link active">
            <h1 style="margin-top:30px; margin-left:-50px;"><?php echo $row['st_name'] ?></h1>
                
            </a>
</a>
                <?php
    }}
                ?>
    </li>


        </ul>
        <ul class="navbar-nav ">
        <?php //check key
        $strSQL="SELECT * FROM owners";
        $result = @$conn->query($strSQL);
            if (@$_SESSION['key'] == md5(@$_SESSION['ow_email'])) {
                $strSQL="SELECT * FROM owners WHERE ow_email = '".$_SESSION['ow_email']."' ";
                $result = @$conn->query($strSQL);
                while($row = $result->fetch_assoc()){
            //stay in this page if have key from login page
            //เข้าสู่ระบบได้จะแสดงหน้าฟอร์มนี้

            echo " 
           
            <li class=\"nav-item\">
            <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"><i class=\"far fa-user-circle\"></i> ผู้ใช้ระบบ : " . @$row['ow_name'] ."  สถานะ : ".$_SESSION['ow_status'].  "</a>
            </li>
             <form id=\"logout\" name=\"logout\">
                 <li class=\"nav-item\">
                 <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"onclick=\"logoutt()\"cursor:pointer><i class=\"fas fa-sign-out-alt\"></i> ออกจากระบบ</a>
                    
                 </li>
        </form>";
                }
            } else {
            //go back to login page.php
            echo "
                        
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#myModaldlogin\" style=\"cursor:pointer\"><i class=\"fas fa-sign-in-alt\"></i> เข้าสู่ระบบ</a>
                        </li>";
                       
            }
    ?>
            <!----><div class="modal " id="myModaldlogin">
                <div class="modal-dialog">
                    <div class="modal-content" align="center">
                        <!-- Modal Header -->
                        <div class="modal-header d-block"style="background-color: #D9E2F3 ;">
                            <h2 class="form-group modal-title" style="font-weight: bold;">Log In</h2>
                        </div>
                        <form id="flogin" name="flogin" method="POST">
                        <!-- Modal body -->
                        <div class="modal-body"style="background-color: #D9E2F3  ;">
                            <img src="https://img.pngio.com/participant-png-png-image-participant-png-512_512.png" width="150px" height="150px">
                                <div class="form-group col-sm-9">
                                    <label for="cus_email" style="font-weight: regular;">Username</label>
                                    <input type="text" placeholder="อีเมล" class="form-control" id="ow_email" name="ow_email">
                                </div>
                                <div class="form-group col-sm-9">
                                    <label for="cus_pass" style="font-weight: regular;">Password</label>
                                    <input type="password" placeholder="รหัสผ่าน" class="form-control" id="ow_pass" name="ow_pass">
                                </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center"style="background-color: #D9E2F3 ;">
                            <button id="mdcolor" class="btn btn-success" type="button" onclick="login()">เข้าสู่ระบบ</button>
                            <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

           
        </ul>
    </nav>


    

    <div class="container">
    <div class="row">
<div class="col-sm-2" style="margin-top:50px">
  <ul class="nav nav-pills flex-column " style="margin-left:-50px;">
    <li class="nav-item" >
                    <?php

                include "setting/config.php";

                $strSQL="SELECT * FROM  store  ORDER BY st_name ASC";

                $result=@$conn->query($strSQL);

                if($result->num_rows>0){
                    while($row=$result->fetch_assoc()){
                ?>
                            <a class="nav-link active" href="#">
                                    <h6><?php echo $row['st_name'] ?></h6>
                                  
                            </a>
                                <?php
                    }}
                                ?>
              
    </li>
    <?php 
    
    if($_SESSION['ow_status']=="เจ้าของกิจการ")
    {
        echo "
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"owner.php\" >ข้อมูลส่วนตัว</a>
        </li>
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../backend/basicinformation.php\">จัดการข้อมูลพื้นฐาน</a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../backend/assess1.php\">รับและประเมินราคาสินค้า</a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">นัดรับสินค้า</a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">ซ่อมและแจ้งรับสินค้า</a>
      </li>  
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">ส่งคืนสินค้า</a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">ประชาสัมพันธ์</a>
      </li>  
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">แจ้งปัญหาการใช้งาน</a>
      </li>";
    }
    if($_SESSION['ow_status']=="ลูกค้า")
    {
        echo "
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">ประชาสัมพันธ์</a>
      </li>  
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">แจ้งปัญหาการใช้งาน</a>
      </li>";
    }

    ?>
  </ul>
  <hr class="d-sm-none">
</div>


  <div class="col-md-8 col-sm-4 font" style="background-color: #C4D8F1  ">   <h1 align="center" class="font" style="padding-top: 20px;">เพิ่มข้อมูลร้าน</h1>

<div class="row" style="margin-right:-90px;">
         <div class="col-md-2"></div>
         <div class="col-md-8">

          <form name="form1" id="form1" method="POST">
              <label class="font" style="font-size:20px">ชื่อร้าน:</label>
              <input type="text" class="form-control" id="stname" name="stname" placeholder="ชื่อ">
                <br>
                <label class="font" style="font-size:20px">เลขที่ผู้เสียภาษี:</label>
              <input type="text" class="form-control" id="st_num" name="st_num" placeholder="เลขที่ผู้เสียภาษี">
                <br> 
              <label class="font" style="font-size:20px">ที่อยู่:</label>
              <textarea type="text" class="form-control" id="stadd" name="stadd" placeholder="ที่อยู่" maxlength="300" minlength="300"></textarea>
              <br>  
              <label class="font" style="font-size:20px">เบอร์โทรศัพท์:</label>
              <input type="text" class="form-control" id="sttel" name="sttel" placeholder="เบอร์โทรศัพท์" maxlength="10" minlength="10">
              <br>
              <label class="font" style="font-size:20px">อัตราค่าบริการ:</label>
              <input type="text" class="form-control" id="stservice" name="stservice" placeholder="อัตราค่าบริการ">
                <br>
                
              </form>
              <div align="center">
                                        <button  type="button" class="btn btn-success" style="margin-right: 50px;" onclick="adddata()">ยืนยัน 
                                         <a href="basicinformation.php"> <button  type="reset" class="btn btn-danger " >ยกเลิก</button></a></button>
                                    
            </div>
    </div>        
</div>   
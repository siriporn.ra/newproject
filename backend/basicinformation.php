<?php
    include "setting/config.php";
?>
<?php
    @session_start();
    @session_cache_expire(30);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>หน้าแรก</title>
    <link rel="stylesheet" href="style.css">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }
        
        .card{
            background-color: #ffffff;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .bg{
            color:#000000  ;
        }


        .navbar{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
   /* height:100vh;*/
    color: #fff;


    width:100%; 
    height:90px;

}
/*************************************************************************************** */

/*************************************************************************************** */


.fo{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:30vh;
    color: #fff;
}

.form-group{
    color:#000000 ;
}


.sidenav {
  height: 72%;
  width: 15%;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #DBDDE7;
  overflow-x: hidden;
  padding-top: 20px;
}

.sidenav a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
}

.sidenav a:hover {
  color: #f1f1f1;
}

    </style>

    
<script>
    function login() {
        document.forms["flogin"].action = "login_process.php";
        document.forms["flogin"].submit();
    }
    function logoutt(){
        document.forms["logout"].action = "/SeniorProject/frontend/index.php";
        document.forms["logout"].submit();
    }


    var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };  
/*******************************ข้อมูลผู้ใช้*************************/
function acceptupdate(){
document.forms["formupdateuser"].action="updateowners.php";
document.forms["formupdateuser"].submit();

  }




function edituser(obj){
 
    var ownid =obj.getAttribute("ow_id");
    var ownname =obj.getAttribute("uname");
    var owntel =obj.getAttribute("utel");
    var ownadd =obj.getAttribute("uadd");
    var ownemail =obj.getAttribute("uemail");
    var ownpass =obj.getAttribute("upass");
    var ownstatus =obj.getAttribute("ustatus");
    var ownservice =obj.getAttribute("uservice");

    document.getElementById("ow_id").value = ownid;
    document.getElementById("uname").value = ownname;
    document.getElementById("utel").value = owntel;
    document.getElementById("utelold").value = owntel;
    document.getElementById("uadd").value = ownadd;
    document.getElementById("uemail").value = ownemail;
    document.getElementById("upass").value = ownpass;
    document.getElementById("ustatus").value = ownstatus;
    document.getElementById("uservice").value = ownservice;

}
/*<!-- ลบข้อมูล -->*/

var globalid;
function sdeletedata(obj){
   
    var protid =obj.getAttribute("delete_id");
    var protname =obj.getAttribute("delete_name");
    globalid=protid

    document.getElementById("deletename").innerHTML = protname;
    document.getElementById("deleteid").innerHTML = protid;
}

function deletedata(){
  //  alert(globalid)
//alert(globalid);
    $.ajax({ //กล่องๆนึงรวมข้อมูลไว้
                type:"POST",
                async :false, //ทำให้ทำงานเรียงตามลำดับ
                url:"webservice/deleteowner.php",
                data:{
                    delete_id:globalid
                }
            });
            location.reload();
}


/*******************************ข้อมูลร้าน*************************/

function acceptupdatestore(){
document.forms["formupdatestore"].action="updatestore.php";
document.forms["formupdatestore"].submit();

  }

function editstroe(obj){
    var stname =obj.getAttribute("sname");
    var stadd =obj.getAttribute("sadd");
    var sttel =obj.getAttribute("stel");
    var stservice =obj.getAttribute("sservice");
    var num_tax =obj.getAttribute("num_tax");
    var stlogo =obj.getAttribute("slogo");
    var st_detail_service =obj.getAttribute("st_detail_service");
    console.log(stlogo);
   // console.log(st_detail_service);

    document.getElementById("sname").value = stname;
    document.getElementById("sadd").value = stadd;
    document.getElementById("stel").value = sttel;
    document.getElementById("utel2").value = sttel;
    document.getElementById("sservice").value = stservice;
    document.getElementById("num_tax").value = num_tax;
   // document.getElementById("slogo").value = stlogo;
    document.getElementById("output").src ="image/"+stlogo;
    document.getElementById("st_detail_service").value =st_detail_service;
    
}
/*<!-- ลบข้อมูล -->*/

var globalid;
function sdeletestore(obj){
   
    var protid =obj.getAttribute("delete_id");
    var protname =obj.getAttribute("delete_name");
    globalid=protid

    document.getElementById("deletename").innerHTML = protname;
    document.getElementById("deleteid").innerHTML = protid;
}

function deletedatastore(){
  //  alert(globalid)
//alert(globalid);
    $.ajax({ //กล่องๆนึงรวมข้อมูลไว้
                type:"POST",
                async :false, //ทำให้ทำงานเรียงตามลำดับ
                url:"webservice/deletestore.php",
                data:{
                    delete_id:globalid
                }
            });
            location.reload();
}

/**********************************************************ข้อมูลประเภทสินค้า************************************************************** */
function acceptupdatepro_type(){
document.forms["formupdatepro_type"].action="updatepro_type.php";
document.forms["formupdatepro_type"].submit();

  }

function editpro_type(obj){
 
    var type_proid =obj.getAttribute("typeid");
    var type_proname =obj.getAttribute("typename");


    document.getElementById("typeid").value = type_proid;
    document.getElementById("typename").value = type_proname;


}


/*<!-- ลบข้อมูล -->*/

var globalid;
function sdeletedatapro_type(obj){
   
    var protid =obj.getAttribute("delete_id");
    var protname =obj.getAttribute("delete_name");
    globalid=protid

    document.getElementById("deletename").innerHTML = protname;
    document.getElementById("deleteid").innerHTML = protid;
}

function deletedatapro_type(){
  //  alert(globalid)
//alert(globalid);
    $.ajax({ //กล่องๆนึงรวมข้อมูลไว้
                type:"POST",
                async :false, //ทำให้ทำงานเรียงตามลำดับ
                url:"webservice/deletepro_type.php",
                data:{
                    delete_id:globalid
                }
            });
            location.reload();
}

/**********************************************************ข้อมูลสินค้า************************************************************** */
function acceptupdateproduct(){
document.forms["formupdateproducr"].action="updateproduct.php";
document.forms["formupdateproducr"].submit();

  }

function editproduct(obj){
    var p_id =obj.getAttribute("pid");
    var p_name =obj.getAttribute("pname");
    var p_brand =obj.getAttribute("pbrand");
    var p_gen =obj.getAttribute("pgen");
    var p_type_id =obj.getAttribute("typeid");


    document.getElementById("pid").value = p_id;
    document.getElementById("pname").value = p_name;
    document.getElementById("pbrand").value = p_brand;
    document.getElementById("pgen").value = p_gen;
    document.getElementById("ptypeid").value = p_type_id;

}

/*<!-- ลบข้อมูล -->*/

var globalid;
function sdeletedataproduct(obj){
   
    var protid =obj.getAttribute("delete_id");
    var protname =obj.getAttribute("delete_name");
    globalid=protid

    document.getElementById("deletename").innerHTML = protname;
    document.getElementById("deleteid").innerHTML = protid;
}

function deletedataproduct(){
  //  alert(globalid)
//alert(globalid);
    $.ajax({ //กล่องๆนึงรวมข้อมูลไว้
                type:"POST",
                async :false, //ทำให้ทำงานเรียงตามลำดับ
                url:"webservice/deleteproduct.php",
                data:{
                    delete_id:globalid
                }
            });
            location.reload();
}
/**********************************************************ข้อมูลประเภทอุปกรณ์************************************************************** */
function acceptupdateeq_type(){
document.forms["formupdateeq_type"].action="updateeq_type.php";
document.forms["formupdateeq_type"].submit();

  }

function editeq_type(obj){
 
    var eqtype_id =obj.getAttribute("eqid");
    var eqtype_name =obj.getAttribute("eqname");


    document.getElementById("eqid").value = eqtype_id;
    document.getElementById("eqname").value = eqtype_name;


}


/*<!-- ลบข้อมูล -->*/

var globalid;
function sdeletedataeq_type(obj){
   
    var protid =obj.getAttribute("delete_id");
    var protname =obj.getAttribute("delete_name");
    globalid=protid

    document.getElementById("deletename").innerHTML = protname;
    document.getElementById("deleteid").innerHTML = protid;
}

function deletedataeq_type(){
  //  alert(globalid)
//alert(globalid);
    $.ajax({ //กล่องๆนึงรวมข้อมูลไว้
                type:"POST",
                async :false, //ทำให้ทำงานเรียงตามลำดับ
                url:"webservice/delete_type_eq.php",
                data:{
                    delete_id:globalid
                }
            });
            location.reload();
}

/**********************************************************ข้อมูลอุปกรณ์************************************************************** */

function acceptupdateequ(){
document.forms["formupdateequ"].action="update_equipment.php";
document.forms["formupdateequ"].submit();

  }

function editequ(obj){
    var equ_id =obj.getAttribute("equid");
    var equ_name =obj.getAttribute("equname");
    var equ_num =obj.getAttribute("equnum");
    var eq_type_id =obj.getAttribute("eq_type_id");
    var eq_detail =obj.getAttribute("equdetail");

console.log(equ_id+equ_name+equ_num+eq_type_id);
  
    document.getElementById("equid").value = equ_id;
    document.getElementById("equname").value = equ_name;
    document.getElementById("equnum").value = equ_num;
    document.getElementById("eq_type_id").value = eq_type_id;
    document.getElementById("equdetail").value = eq_detail;


}

/*<!-- ลบข้อมูล -->*/

var globalid;
function sdeletedataequ(obj){
   
    var protid =obj.getAttribute("delete_id");
    var protname =obj.getAttribute("delete_name");
    globalid=protid

    document.getElementById("deletename").innerHTML = protname;
    document.getElementById("deleteid").innerHTML = protid;
}

function deletedataequ(){
  //  alert(globalid)
//alert(globalid);
    $.ajax({ //กล่องๆนึงรวมข้อมูลไว้
                type:"POST",
                async :false, //ทำให้ทำงานเรียงตามลำดับ
                url:"webservice/delete_equipment.php",
                data:{
                    delete_id:globalid
                }
            });
            location.reload();
}

</script>

    <body>
    <nav class="navbar navbar-expand-sm" >
                  

        <ul class="navbar-nav mr-auto" >

        <?php

include "setting/config.php";
$strSQL="SELECT * FROM  store  ORDER BY st_name ASC";
$result=@$conn->query($strSQL);
if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
?>
<a class="navbar-brand" href="/SeniorProject/frontend/index2.php">
        <img  src="../backend/image/<?php echo $row['st_logo'] ?>"width="200px;" height="120%" style="margin-left:-50px; ">   

            <a class="nav-link active">
            <h1 style="margin-top:30px; margin-left:-50px;"><?php echo $row['st_name'] ?></h1>
                
            </a>
</a>
                <?php
    }}
                ?>
    
        </ul>

                    










        <ul class="navbar-nav ">
        <?php //check key
        $strSQL="SELECT * FROM owners";
        $result = @$conn->query($strSQL);
            if (@$_SESSION['key'] == md5(@$_SESSION['ow_email'])) {
                $strSQL="SELECT * FROM owners WHERE ow_email = '".$_SESSION['ow_email']."' ";
                $result = @$conn->query($strSQL);
                while($row = $result->fetch_assoc()){
            //stay in this page if have key from login page
            //เข้าสู่ระบบได้จะแสดงหน้าฟอร์มนี้

            echo " 
           
            <li class=\"nav-item\">
            <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"><i class=\"far fa-user-circle\"></i> ผู้ใช้ระบบ : " . @$row['ow_name'] ."  สถานะ : ".$_SESSION['ow_status'].  "</a>
            </li>
             <form id=\"logout\" name=\"logout\">
                 <li class=\"nav-item\">
                 <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"onclick=\"logoutt()\"cursor:pointer><i class=\"fas fa-sign-out-alt\"></i> ออกจากระบบ</a>
                    
                 </li>
        </form>";
                }
            } else {
            //go back to login page.php
            echo "
                        
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#myModaldlogin\" style=\"cursor:pointer\"><i class=\"fas fa-sign-in-alt\"></i> เข้าสู่ระบบ</a>
                        </li>";
                       
            }
    ?>
            <!----><div class="modal " id="myModaldlogin">
                <div class="modal-dialog">
                    <div class="modal-content" align="center">
                        <!-- Modal Header -->
                        <div class="modal-header d-block"style="background-color: #D9E2F3 ;">
                            <h2 class="form-group modal-title" style="font-weight: bold;">Log In</h2>
                        </div>
                        <form id="flogin" name="flogin" method="POST">
                        <!-- Modal body -->
                        <div class="modal-body"style="background-color: #D9E2F3  ;">
                            <img src="https://img.pngio.com/participant-png-png-image-participant-png-512_512.png" width="150px" height="150px">
                                <div class="form-group col-sm-9">
                                    <label for="cus_email" style="font-weight: regular;">Username</label>
                                    <input type="text" placeholder="อีเมล" class="form-control" id="ow_email" name="ow_email">
                                </div>
                                <div class="form-group col-sm-9">
                                    <label for="cus_pass" style="font-weight: regular;">Password</label>
                                    <input type="password" placeholder="รหัสผ่าน" class="form-control" id="ow_pass" name="ow_pass">
                                </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer d-block" align="center"style="background-color: #D9E2F3 ;">
                            <button id="mdcolor" class="btn btn-success" type="button" onclick="login()">เข้าสู่ระบบ</button>
                            <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

           
        </ul>
    </nav>


    

    <div class="container  col-sm-12 ">
    <div class="row">
<div class="col-sm-2" style="margin-top:90px">
  <ul class="nav nav-pills flex-column">
    <li class="nav-item" >
                <?php

            include "setting/config.php";

            $strSQL="SELECT * FROM  store  ORDER BY st_name ASC";

            $result=@$conn->query($strSQL);

            if($result->num_rows>0){
                while($row=$result->fetch_assoc()){
            ?>
                        <a class="nav-link active" href="#">
                                <h6><?php echo $row['st_name'] ?></h6>
                              
                        </a>
                            <?php
                }}
                            ?>
    
    </li>
    <?php 
    
    if($_SESSION['ow_status']=="เจ้าของกิจการ")
    {
        echo "
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"owner.php\" >ข้อมูลส่วนตัว</a>
        </li>
        <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"../backend/basicinformation.php\">จัดการข้อมูลพื้นฐาน</a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"show_listrepair.php\">รับและประเมินราคาสินค้า</a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"repair_record.php\">ซ่อมและแจ้งรับสินค้า</a>
      </li>  
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">ส่งคืนสินค้า</a>
      </li>
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">ประชาสัมพันธ์</a>
      </li>  
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">แจ้งปัญหาการใช้งาน</a>
      </li>";
    }
    if($_SESSION['ow_status']=="ลูกค้า")
    {
        echo "
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">ประชาสัมพันธ์</a>
      </li>  
      <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"#\">แจ้งปัญหาการใช้งาน</a>
      </li>";
    }

    ?>
  </ul>
  <hr class="d-sm-none">
</div>
    <div class="container col-sm-10">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist" id="uif">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#info">ข้อมูลผู้ใช้งาน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#store">ข้อมูลร้าน</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#barterlist">ข้อมูลประเภทสินค้า</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#pro_infor">ข้อมูลสินค้า</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#eq_type">ข้อมูลประเภทอุปกรณ์</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#eq">ข้อมูลอุปกรณ์</a>
            </li>
        </ul>

<!---**************************************************ข้อมูลผู้ใช้งาน********************************************************-->

        <div class="tab-content">
            
                <div id="info" class="container tab-pane active"><br><br>
                    <h3>เจ้าของกิจการ </h3>

                    <table class="table table-bordered font col-sm-12">
   
                                <tr class="t col-sm-12" align="center" style="font-size:18px;">
                                    <th >ชื่อ-นามสกุล</th>
                                    <th  >ที่อยู่</th>
                                    <th  >เบอร์โทรศัพท์</th>
                                    <th  >E-mail</th>
                                    <th  >สถานะ</th>
                                    <th  >รหัสผ่าน</th>
                                    <th  width="10%">
                                    <label ><b><a href="add_owners.php">
                                    <img src="image/add11.png" wigth="35px" height="35px"></a></b></label>
                                    </th>
                                </tr> 

        <?php
          $strSQL="SELECT * FROM owners WHERE ow_status = 'เจ้าของกิจการ' AND ow_id != '".$_SESSION['ow_id']."' ORDER BY ow_id ASC" ;
          $result=@$conn->query($strSQL);
        ?>

<?php
  if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
?>

                    <tr style="font-size:15px;" class="font" >
                        <td><?php echo $row['ow_name']; ?></td> 
                        <td><?php echo $row['ow_add']; ?></td> 
                        <td><?php echo $row['ow_tel']; ?></td> 
                        <td><?php echo $row['ow_email']; ?></td> 
                        <td><?php echo $row['ow_status']; ?></td> 
                        <td>************</td> 
                        <td align="center">
                    <a data-toggle="modal" data-target="#edit" 
                         utel="<?php echo $row['ow_tel'];  ?>"
                         uname="<?php echo $row['ow_name'];  ?>"
                         uadd="<?php echo $row['ow_add'];?>"
                         uemail="<?php echo $row['ow_email'];  ?>"
                         ustatus="<?php echo $row['ow_status'];  ?>"
                         upass="<?php echo $row['ow_pass'];  ?>"
                         uservice="<?php echo $row['ow_service_rates'];  ?>"
                         onclick="edituser(this)" style="cursor:pointer;">
                         <img src="image/edit_new.png" wigth="35px" height="35px"></a>
    <a  data-toggle="modal" data-target="#myModal1" 
                        delete_id="<?php echo $row['ow_tel']; ?>"
                        delete_name="<?php echo " ชื่อ : ".$row['ow_name']. " เบอร์ : ".$row['ow_tel']; ?>" 
                        onclick="sdeletedata(this)"style="cursor:pointer;" >
                        <img src="image/delete.png" wigth="35px" height="35px"></a>

                                          
                      </td>
                </tr>
      <?php
      
    }}
    ?>
      </table>

                    <h3>ลูกค้า </h3>

                    <table class="table table-bordered font col-sm-12">
   
                                <tr class="t col-sm-12" align="center" style="font-size:18px;">
                                    <th >ชื่อ-นามสกุล</th>
                                    <th  >ที่อยู่</th>
                                    <th  >เบอร์โทรศัพท์</th>
                                    <th  >E-mail</th>
                                    <th  >สถานะ</th>
                                    <th  >รหัสผ่าน</th>
                                    <th  width="10%">
                                    <label ><b><a href="add_owners.php">
                                    <img src="image/add11.png" wigth="35px" height="35px"></a></b></label>
                                    </th>
                                </tr> 


                                <?php
    $strSQL="SELECT * FROM  owners WHERE ow_status = 'ลูกค้า' AND ow_name != '".$_SESSION['ow_name']."' ORDER BY ow_id ASC" ;

    $result=@$conn->query($strSQL);

      ?>

  
<?php



if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
      ?>

                    <tr style="font-size:15px;" class="font" >
                        <td><?php echo $row['ow_name']; ?></td> 
                        <td><?php echo $row['ow_add']; ?></td> 
                        <td><?php echo $row['ow_tel']; ?></td> 
                        <td><?php echo $row['ow_email']; ?></td> 
                        <td><?php echo $row['ow_status']; ?></td> 
                        <td>************</td> 
                        <td align="center">
                        
    <a data-toggle="modal" data-target="#edit" 
                         utel="<?php echo $row['ow_tel'];  ?>"
                         uname="<?php echo $row['ow_name'];  ?>"
                         uadd="<?php echo $row['ow_add'];?>"
                         uemail="<?php echo $row['ow_email'];  ?>"
                         ustatus="<?php echo $row['ow_status'];  ?>"
                         upass="<?php echo $row['ow_pass'];  ?>"
                         uservice="<?php echo $row['ow_service_rates'];  ?>"
                         onclick="edituser(this)" style="cursor:pointer;"> 
                         <img src="image/edit_new.png" wigth="35px" height="35px"></a>
                  
    <a  data-toggle="modal" data-target="#myModal1" 
                        delete_id="<?php echo $row['ow_tel']; ?>"
                        delete_name="<?php echo " ชื่อ : ".$row['ow_name']. " เบอร์ : ".$row['ow_tel']; ?>" 
                        onclick="sdeletedata(this)"style="cursor:pointer;" >
                        <img src="image/delete.png" wigth="35px" height="35px"></a>
                      </td>
                </tr>
      <?php
      
    }}
?>
</table>
</div>    

<!--แก้ไขข้อมูล-->
<div class="modal" id="edit" >
    <div class="modal-dialog modal-lg" style="border:4px #FF6D33  solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header d-block">
          <h2 class="modal-title font" style="font-weight: bold;" align="center">แก้ไขข้อมูลผู้ใช้งาน</h2>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body font">
        <center><img src="image/edit-valid.png" wigth="200px" height="200px" ></center>
        <br>
        <form id="formupdateuser" name="formupdateuser" method="POST">
              <label class="font" style="font-size: 20px;">ชื่อ:</label>
              <input hidden type="text" class="form-control" id="ow_id" name="ow_id" placeholder="ไอดี(ซ่อน)">
              <input type="text" class="form-control" id="uname" name="uname" placeholder="ชื่อ">
            <br>
              <label class="font" style="font-size: 20px;">เบอร์โทรศัพท์:</label>
              <input  type="text" class="form-control" id="utel" name="utel" placeholder="เบอร์โทรศัพท์" maxlength="10" minlength="10">
              <input   type="text" class="form-control" id="utelold" name="utelold" hidden maxlength="10" minlength="10">
            <br>
              <label class="font" style="font-size: 20px;">สถานะ:</label>
              <select class="form-control"  id="ustatus" name="ustatus"  >
                <option value="เจ้าของกิจการ">เจ้าของกิจการ</option>
                <option value="ลูกค้า">ลูกค้า</option>
              </select>                                        <br>
              <label class="font" style="font-size: 20px;">ที่อยู่:</label>
              <textarea type="text" class="form-control" id="uadd" name="uadd" placeholder="ที่อยู่"></textarea>
            <br>
              <label class="font" style="font-size: 20px;">อีเมล:</label>
              <input type="text" class="form-control" id="uemail" name="uemail" placeholder="อีเมล"></input>
            <br>
              <label class="font" style="font-size: 20px;">รหัสผ่าน:</label>
              <input type="password" class="form-control" id="upass" name="upass" placeholder="รหัสผ่าน"></input>
            <br>
                                     
                                      <!-- Modal footer -->
        <div class="modal-footer d-block" align="center"  style="background-color: #E2FA6E;">
          <button id="mdcolor" class="btn btn-success" type="button" data-dismiss="modal" onclick="acceptupdate()">ยืนยันการแก้ไข</button>
          <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
        </div>
      </div>
    </div>
  </div>
</div>

                                  <!-- Delete ลบข้อมูล -->
<div class="modal" id="myModal1">
  <div class="modal-dialog modal-lg" style="border:4px #F2D1D1   solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
      <div class="modal-content">
        
        <!-- Modal body -->
        <div class="modal-body d-block" align="center" style="background-color:#DFE6F4;">
            <img src="image/delete12.png" width="100px" height="100px"><br><br><br>
            <h2><b>ลบข้อมูล</b></h2><br>
            <p>คุณต้องการลบข้อมูล <b id="deletename"></b> หรือไม่?? </p><br>
            <form id="form2" name="form2" method="POST">
            <button type="button" class="btn btn-success" data-dismiss="modal"  onclick="deletedata()"  >ยืนยัน</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
            </form>
        </div>
      </div>
    </div>
  </div>



<!--*************************************************************************ข้อมูลร้าน***************************************************-->
<!--ข้อมูลร้าน-->
        <div id="store" class="container tab-pane fade "><br>
                    <h3>ข้อมูลร้าน</h3> 
            <table class="table table-bordered font col-sm-12">
   
                        <tr class="t" align="center" style="font-size:18px;">
                            <th >ชื่อร้าน</th>
                            <th  >ที่อยู่</th>
                            <th  >เบอร์โทรศัพท์</th>
                            <th  >อัตราค่าบริการ</th>
                            <th  >โลโก้ร้าน</th>
                            <th  >เลขที่ผู้เสียภาษี</th>
                            <th  >รายละเอียดการให้บริการ</th>
                            <th  width="10%"> </th>
                          
                        </tr>
                  
                  <?php
                $strSQL="SELECT * FROM  store  ORDER BY st_name ASC" ;

                $result=@$conn->query($strSQL);

                  ?>


                <?php

                if($result->num_rows>0){
                while($row=$result->fetch_assoc()){
                  ?>

                 <tr style="font-size:15px;" class="font" >
                     <td><?php echo $row['st_name']; ?></td> 
                     <td><?php echo $row['st_add']; ?></td> 
                     <td><?php echo $row['st_tel']; ?></td> 
                     <td><?php echo $row['st_service_rates']; ?></td> 
                     <td><?php echo $row['st_logo']; ?></td> 
                     <td><?php echo $row['st_num_tax']; ?></td> 
                     <td><?php echo $row['st_detail_service']; ?></td> 
                     <td align="center">

    <a data-toggle="modal" data-target="#edit2" 
                      sname="<?php echo $row['st_name'];  ?>"
                      sadd="<?php echo $row['st_add'];  ?>"
                      stel="<?php echo $row['st_tel'];  ?>"
                      sservice="<?php echo $row['st_service_rates'];  ?>"
                      slogo="<?php echo $row['st_logo'];  ?>"
                      num_tax="<?php echo $row['st_num_tax'];  ?>"
                      st_detail_service="<?php echo $row['st_detail_service'];  ?>"
                      onclick="editstroe(this)" style="cursor:pointer;">
                      <img src="image/edit_new.png" wigth="35px" height="35px"></a>
    
</div>                                            
                   </td>
             </tr>
   <?php
   
 }}
?>

                                   <!-- Delete ลบข้อมูล -->
<div class="modal" id="myModal2">
<div class="modal-dialog modal-lg" style="border:4px #F2D1D1   solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
   <div class="modal-content">
     
     <!-- Modal body -->
     <div class="modal-body d-block" align="center" style="background-color:#DFE6F4;">
         <img src="image/delete12.png" width="100px" height="100px"><br><br><br>
         <h2><b>ลบข้อมูล</b></h2><br>
         <p>คุณต้องการลบข้อมูล <b id="deletename"></b> หรือไม่?? </p><br>
         <form id="form2" name="form2" method="POST">
         <button type="button" class="btn btn-success" data-dismiss="modal"  onclick="deletedatastore()"  >ยืนยัน</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
         </form>
     </div>
   </div>
 </div>
</div>




<!--แก้ไขข้อมูล-->
<div class="modal" id="edit2" >
 <div class="modal-dialog modal-lg" style="border:4px #FF6D33  solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
   <div class="modal-content">
   
     <!-- Modal Header -->
     <div class="modal-header d-block">
       <h2 class="modal-title font" style="font-weight: bold;" align="center">แก้ไขข้อมูลร้าน</h2>
     </div>
     
     <!-- Modal body -->
     <div class="modal-body font">
     <center><img src="image/edit-valid.png" wigth="200px" height="200px" ></center>
     <br>
     <form id="formupdatestore" name="formupdatestore" method="POST" enctype="multipart/form-data">
                                     <label class="font" style="font-size: 20px;">ชื่อร้าน:</label>
                                     <input type="text" class="form-control" id="sname" name="sname" placeholder="ชื่อ">
                                     <br>
                                     <label class="font" style="font-size: 20px;">ที่อยู่:</label>
                                     <textarea type="text" class="form-control" id="sadd" name="sadd" placeholder="ที่อยู่"></textarea>
                                     <br>
                                     <label class="font" style="font-size: 20px;">เบอร์โทรศัพท์:</label>
                                     <input  type="text" class="form-control" id="stel" name="stel" placeholder="เบอร์โทรศัพท์" maxlength="10" minlength="10">
                                     <input hidden type="text" class="form-control" id="utel2" name="utel2" placeholder="เบอร์โทรศัพท์" maxlength="10" minlength="10">
                                     <br>
                                     <label class="font" style="font-size: 20px;">อัตราค่าบริการ:</label>
                                     <input type="text" class="form-control" id="sservice" name="sservice" placeholder="อัตราค่าบริการ">
                                     <br>
                                     <br>
                                     <label class="font" style="font-size: 20px;">เลขที่ผู้เสียภาษี:</label>
                                     <input type="text" class="form-control" id="num_tax" name="num_tax" minlength="13"maxlength="13" placeholder="เลขที่ผู้เสียภาษี">
                                     <br>
                                     <label class="font" style="font-size: 20px;">รายละเอียดการให้บริการ:</label>
                                     <input type="text" class="form-control" id="st_detail_service" name="st_detail_service"  placeholder="รายละเอียดการให้บริการ">
                                     <br>  
                                     <label class="font" style="font-size: 20px;">โลโก้:</label>
                                     <br><label for="usr">กรุณาอัพโหลดรูปสินค้า:<b style="color:red;">*ถ้าไม่เปลี่ยนไม่ต้องอัพโหลด</b></label><br>
                                         <input name="slogo" id="slogo" type="file" onchange="loadFile(event)"  accept="image/*" >
                                         <br>
                                         <img id="output" name="output" width="100px" weight="100px" src="/backend/image/*" />  
                                         
                                                                   <br>
                                   <!-- Modal footer -->
     <div class="modal-footer d-block" align="center"  style="background-color: #E2FA6E;">
       <button id="mdcolor" class="btn btn-success" type="button" data-dismiss="modal" onclick="acceptupdatestore()">ยืนยันการแก้ไข</button>
       <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
     </div>
   </div>
 </div>
</div>

</table>
                </div>

    

<!--*************************************************************************ข้อมูลประเภทสินค้า***************************************************-->
<div id="barterlist" class="container tab-pane fade "><br>
                    <h3>ข้อมูลประเภทสินค้า</h3>
                    <table class="table table-bordered font col-sm-12">
   
   <tr class="t" align="center" style="font-size:18px;">
     <th >รหัสประเภทสินค้า</th>
     <th  >ชื่อประเภทสินค้า</th>
     <th  width="10%">
     <label ><b><a href="add_product_type.php">
     <img src="image/add11.png" wigth="35px" height="35px"></a> </button></b></label>
     </th>
   </tr>
   
              <?php
            $strSQL="SELECT * FROM  product_type  ORDER BY pro_type_id ASC" ;

            $result=@$conn->query($strSQL);

              ?>


            <?php

            if($result->num_rows>0){
            while($row=$result->fetch_assoc()){
              ?>

                 <tr style="font-size:15px;" class="font" >
                     <td><?php echo $row['pro_type_id']; ?></td> 
                     <td><?php echo $row['pro_type_name']; ?></td> 
                
                     <td align="center">

 <a data-toggle="modal" data-target="#edit3" 
                      typeid ="<?php echo $row['pro_type_id'];  ?>"
                      typename="<?php echo $row['pro_type_name'];  ?>"
                      onclick="editpro_type(this)" style="cursor:pointer;">
                      <img src="image/edit_new.png" wigth="35px" height="35px"></a>
 <a  data-toggle="modal" data-target="#myModal3" 
                     delete_id="<?php echo $row['pro_type_id']; ?>"
                     delete_name="<?php echo " ประเภทสินค้า : ".$row['pro_type_name']; ?>" 
                     onclick="sdeletedatapro_type(this)"style="cursor:pointer;" >
                     <img src="image/delete.png" wigth="35px" height="35px"></a>

</div>                                            
                   </td>
             </tr>
   <?php
   
 }}
?>
   </div>
</div>
</div>    

                                   <!-- Delete ลบข้อมูล -->
<div class="modal" id="myModal3">
<div class="modal-dialog modal-lg" style="border:4px #F2D1D1   solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
   <div class="modal-content">
     
     <!-- Modal body -->
     <div class="modal-body d-block" align="center" style="background-color:#DFE6F4;">
         <img src="image/delete12.png" width="100px" height="100px"><br><br><br>
         <h2><b>ลบข้อมูล</b></h2><br>
         <p>คุณต้องการลบข้อมูล <b id="deletename"></b> หรือไม่?? </p><br>
         <form id="form2" name="form2" method="POST">
         <button type="button" class="btn btn-success" data-dismiss="modal"  onclick="deletedatapro_type()"  >ยืนยัน</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
         </form>
     </div>
   </div>
 </div>
</div>




<!--แก้ไขข้อมูล-->
<div class="modal" id="edit3" >
 <div class="modal-dialog modal-lg" style="border:4px #FF6D33  solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
   <div class="modal-content">
   
     <!-- Modal Header -->
     <div class="modal-header d-block">
       <h2 class="modal-title font" style="font-weight: bold;" align="center">แก้ไขข้อมูลประเภทสินค้า</h2>
     </div>
     
     <!-- Modal body -->
     <div class="modal-body font">
     <center><img src="image/edit-valid.png" wigth="200px" height="200px" ></center>
     <br>
     <form id="formupdatepro_type" name="formupdatepro_type" method="POST">
                                   <label class="font" style="font-size: 20px;">รหัสประเภทสินค้า:</label>
                                     <input  type="text" class="form-control" id="typeid" name="typeid" placeholder="ชื่อ">
                                       <br>
                                     <label class="font" style="font-size: 20px;">ชื่อประเภทสินค้า:</label>
                                     <input  type="text" class="form-control" id="typename" name="typename" placeholder="เบอร์โทรศัพท์">
                                     <br>
                                  
                                   <!-- Modal footer -->
     <div class="modal-footer d-block" align="center"  style="background-color: #E2FA6E;">
       <button id="mdcolor" class="btn btn-success" type="button" data-dismiss="modal" onclick="acceptupdatepro_type()">ยืนยันการแก้ไข</button>
       <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
     </div>
   </div>
 </div>
</table>

</div>

<!--*************************************************************************ข้อมูลสินค้า***************************************************-->
<div id="pro_infor" class="container tab-pane fade "><br>
                    <h3>ข้อมูลสินค้า </h3>
                    <table class="table table-bordered font col-sm-12" >
   
      <tr class="t" align="center" style="font-size:18px;">
        <th >รหัสสินค้า</th>
        <th  >ชื่อสินค้า</th>
        <th  >ยี่ห้อ</th>
        <th  >รุ่น</th>
        <th  >ประเภท</th>
        <th  width="10%">
        <label ><b><a href="add_product.php">
        <img src="image/add11.png" wigth="35px" height="35px"></a> </button></b></label>
        </th>
      </tr>
      
      <?php
    $strSQL="SELECT * FROM product INNER JOIN product_type ON product_type.pro_type_id = product.pro_type_id" ;

    $result=@$conn->query($strSQL);

      ?>

  
<?php

if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
      ?>

                    <tr style="font-size:15px;" class="font" >
                        <td><?php echo $row['pro_id']; ?></td> 
                        <td><?php echo $row['pro_name']; ?></td> 
                        <td><?php echo $row['pro_brand']; ?></td> 
                        <td><?php echo $row['pro_generation']; ?></td> 
                        <td><?php echo $row['pro_type_name']; ?></td>  
                        <td align="center">

    <a data-toggle="modal" data-target="#edit4" 
                         pid="<?php echo $row['pro_id'];  ?>"
                         pname="<?php echo $row['pro_name'];  ?>"
                         pbrand="<?php echo $row['pro_brand'];  ?>"
                         pgen="<?php echo $row['pro_generation'];  ?>"
                         typeid="<?php echo $row['pro_type_id'];  ?>"
                         onclick="editproduct(this)" style="cursor:pointer;">
                         <img src="image/edit_new.png" wigth="35px" height="35px"></a>
    <a  data-toggle="modal" data-target="#myModal4" 
                        delete_id="<?php echo $row['pro_id']; ?>"
                        delete_name="<?php echo " ชื่อ : ".$row['pro_name']; ?>" 
                        onclick="sdeletedataproduct(this)"style="cursor:pointer;" >
                        <img src="image/delete.png" wigth="35px" height="35px"></a>

</div>                                            
                      </td>
                </tr>
      <?php
      
    }}
?>
      </div>
</div>
</div>    

                                      <!-- Delete ลบข้อมูล -->
<div class="modal" id="myModal4">
  <div class="modal-dialog modal-lg" style="border:4px #F2D1D1   solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
      <div class="modal-content">
        
        <!-- Modal body -->
        <div class="modal-body d-block" align="center" style="background-color:#DFE6F4;">
            <img src="image/delete12.png" width="100px" height="100px"><br><br><br>
            <h2><b>ลบข้อมูล</b></h2><br>
            <p>คุณต้องการลบข้อมูล <b id="deletename"></b> หรือไม่?? </p><br>
            <form id="form2" name="form2" method="POST">
            <button type="button" class="btn btn-success" data-dismiss="modal"  onclick="deletedataproduct()"  >ยืนยัน</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
            </form>
        </div>
      </div>
    </div>
  </div>




<!--แก้ไขข้อมูล-->
<div class="modal" id="edit4" >
    <div class="modal-dialog modal-lg" style="border:4px #FF6D33  solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header d-block">
          <h2 class="modal-title font" style="font-weight: bold;" align="center">แก้ไขข้อมูลสินค้า</h2>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body font">
        <center><img src="image/edit-valid.png" wigth="200px" height="200px" ></center>
        <br>
        <form id="formupdateproducr" name="formupdateproducr" method="POST">
                                        <label class="font" style="font-size: 20px;">รหัสสินค้า:</label>
                                        <input  type="text" class="form-control" id="pid" name="pid" placeholder="รหัสสินค้า">
                                        <br>
                                        <label class="font" style="font-size: 20px;">ชื่อสินค้า:</label>
                                        <textarea type="text" class="form-control" id="pname" name="pname" placeholder="ชื่อสินค้า"></textarea>
                                        <br>
                                        <label class="font" style="font-size: 20px;">ยี่ห้อ:</label>
                                        <select class="form-control" name="pbrand" id="pbrand" style="width:100%; height:40px; text-align : right; font-size:16px" >
              <?php
                                    $strSQL="SELECT * FROM  brand ORDER BY brand_name ASC" ;
                                    $result=@$conn->query($strSQL);
                                    if($result->num_rows>0){
                                    while($row=$result->fetch_assoc()){
                                                ?>
                                    <option  value="<?php echo trim($row['brand_name']);?>">
                                    <?php echo $row['brand_name'];?>
                                    </option>
                                    <?php
                                    }}
                                    ?>
                 </select>                                         <br>
                                        <label class="font" style="font-size: 20px;">รุ่น:</label>
                                        <textarea type="text" class="form-control" id="pgen" name="pgen" placeholder="รุ่น"></textarea>
                                        <br>
                                        <label class="font" style="font-size: 20px;">ประเภทสินค้า:</label>
                                        <select class="form-control"  id="ptypeid" name="ptypeid"style="font-size:16px;">
                                                            <?php
                                                            $strSQL="SELECT * FROM  product_type ORDER BY pro_type_id ASC" ;
                                                            $result=@$conn->query($strSQL);
                                                            if($result->num_rows>0){
                                                            while($row=$result->fetch_assoc()){
                                                                        ?>
                                                            <option  value="<?php echo trim($row['pro_type_id']);?>">
                                                            <?php echo $row['pro_type_name'];?>
                                                            </option>
                                                            <?php
                                                            }}
                                                            ?>
                                        </select>
                                        <br>
                                       
                                      <!-- Modal footer -->
        <div class="modal-footer d-block" align="center"  style="background-color: #E2FA6E;">
          <button id="mdcolor" class="btn btn-success" type="button" data-dismiss="modal" onclick="acceptupdateproduct()">ยืนยันการแก้ไข</button>
          <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
        </div>
      </div>
    </div>
        </table>
  </div>

<!--*************************************************************************ข้อมูลประเภทอุปกรณ์***************************************************-->
<div id="eq_type" class="container tab-pane fade "><br>
                    <h3>ข้อมูลประเภทอุปกรณ์</h3>
                    <table class="table table-bordered font col-sm-12">
   
   <tr class="t" align="center" style="font-size:18px;">
     <th >รหัสประเภทอุปกรณ์</th>
     <th  >ชื่อประเภทอุปกรณ์</th>
     <th  width="10%">
     <label ><b><a href="add_eq_type.php">
     <img src="image/add11.png" wigth="35px" height="35px"></a> </button></b></label>
     </th>
   </tr>
   
   <?php
 $strSQL="SELECT * FROM  eq_type  ORDER BY eq_type_id  ASC" ;

 $result=@$conn->query($strSQL);

   ?>


<?php

if($result->num_rows>0){
 while($row=$result->fetch_assoc()){
   ?>

                 <tr style="font-size:15px;" class="font" >
                     <td><?php echo $row['eq_type_id']; ?></td> 
                     <td><?php echo $row['eq_type_name']; ?></td> 
                
                     <td align="center">

 <a data-toggle="modal" data-target="#edit5" 
                      eqid ="<?php echo $row['eq_type_id'];  ?>"
                      eqname="<?php echo $row['eq_type_name'];  ?>"
                      onclick="editeq_type(this)" style="cursor:pointer;">
                      <img src="image/edit_new.png" wigth="35px" height="35px"></a>
 <a  data-toggle="modal" data-target="#myModal5" 
                     delete_id="<?php echo $row['eq_type_id']; ?>"
                     delete_name="<?php echo " ประเภทสินค้า : ".$row['eq_type_name']; ?>" 
                     onclick="sdeletedataeq_type(this)"style="cursor:pointer;" >
                     <img src="image/delete.png" wigth="35px" height="35px"></a>

</div>                                            
                   </td>
             </tr>
   <?php
   
 }}
?>
   </div>
</div>
</div>    

                                   <!-- Delete ลบข้อมูล -->
<div class="modal" id="myModal5">
<div class="modal-dialog modal-lg" style="border:4px #F2D1D1   solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
   <div class="modal-content">
     
     <!-- Modal body -->
     <div class="modal-body d-block" align="center" style="background-color:#DFE6F4;">
         <img src="image/delete12.png" width="100px" height="100px"><br><br><br>
         <h2><b>ลบข้อมูล</b></h2><br>
         <p>คุณต้องการลบข้อมูล <b id="deletename"></b> หรือไม่?? </p><br>
         <form id="form2" name="form2" method="POST">
         <button type="button" class="btn btn-success" data-dismiss="modal"  onclick="deletedataeq_type()"  >ยืนยัน</button>
         <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
         </form>
     </div>
   </div>
 </div>
</div>




<!--แก้ไขข้อมูล-->
<div class="modal" id="edit5" >
 <div class="modal-dialog modal-lg" style="border:4px #FF6D33  solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
   <div class="modal-content">
   
     <!-- Modal Header -->
     <div class="modal-header d-block">
       <h2 class="modal-title font" style="font-weight: bold;" align="center">แก้ไขข้อมูลประเภทอุปกรณ์</h2>
     </div>
     
     <!-- Modal body -->
     <div class="modal-body font">
     <center><img src="image/edit-valid.png" wigth="200px" height="200px" ></center>
     <br>
     <form id="formupdateeq_type" name="formupdateeq_type" method="POST">
                                   <label class="font" style="font-size: 20px;">รหัสประเภทอุปกรณ์:</label>
                                     <input type="text" class="form-control" id="eqid" name="eqid" placeholder="ประเภทอุปกรณ์">
                                       <br>
                                     <label class="font" style="font-size: 20px;">ประเภทอุปกรณ์:</label>
                                     <input  type="text" class="form-control" id="eqname" name="eqname" placeholder="ชื่อประเภทอุปกรณ์">
                      
                                  </input>
                                    
                                     <br>
                                  
                                   <!-- Modal footer -->
     <div class="modal-footer d-block" align="center"  style="background-color: #E2FA6E;">
       <button id="mdcolor" class="btn btn-success" type="button" data-dismiss="modal" onclick="acceptupdateeq_type()">ยืนยันการแก้ไข</button>
       <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
     </div>
   </div>
 </div>
</table>

</div>

<!--*************************************************************************ข้อมูล++อุปกรณ์***************************************************-->
<div id="eq" class="container tab-pane fade "><br>
                    <h3>ข้อมูลอุปกรณ์ </h3>
                    <table class="table table-bordered font col-sm-12" >
   
      <tr class="t" align="center" style="font-size:18px;">
        <th  >รหัสอุปกรณ์</th>
        <th  >ชื่ออุปกรณ์</th>
        <th  >หน่วยนับ</th>
        <th  >ประเภทอุปกรณ์</th>
        <th  >รายละเอียด</th>
        <th  width="10%">
        <label ><b><a href="add_equipment.php">
        <img src="image/add11.png" wigth="35px" height="35px"></a> </button></b></label>
        </th>
      </tr>
      
      <?php
    $strSQL="SELECT * FROM equipment INNER JOIN eq_type ON eq_type.eq_type_id = equipment.eq_type_id " ;

    $result=@$conn->query($strSQL);

      ?>

  
<?php

if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
      ?>

                    <tr style="font-size:15px;" class="font" >
                        <td><?php echo $row['eq_id']; ?></td> 
                        <td><?php echo $row['eq_name']; ?></td> 
                        <td><?php echo $row['eq_num']; ?></td> 
                        <td><?php echo $row['eq_type_name']; ?></td> 
                        <td><?php echo $row['eq_detail']; ?></td> 
                        <td align="center">

    <a data-toggle="modal" data-target="#edit6" 
                         equid="<?php echo $row['eq_id'];  ?>"
                         equname="<?php echo $row['eq_name'];  ?>"
                         equnum="<?php echo $row['eq_num'];  ?>"
                         eq_type_id="<?php echo $row['eq_type_id'];  ?>"
                         equdetail="<?php echo $row['eq_detail'];  ?>"
                         onclick="editequ(this)" style="cursor:pointer;">
                         <img src="image/edit_new.png" wigth="35px" height="35px"></a>
    <a  data-toggle="modal" data-target="#myModal6" 
                        delete_id="<?php echo $row['eq_id']; ?>"
                        delete_name="<?php echo " ชื่อ : ".$row['eq_name']; ?>" 
                        onclick="sdeletedataequ(this)"style="cursor:pointer;" >
                        <img src="image/delete.png" wigth="35px" height="35px"></a>

</div>                                            
                      </td>
                </tr>
      <?php
      
    }}
?>
      </div>
</div>
</div>    

                                      <!-- Delete ลบข้อมูล -->
<div class="modal" id="myModal6">
  <div class="modal-dialog modal-lg" style="border:4px #F2D1D1   solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
      <div class="modal-content">
        
        <!-- Modal body -->
        <div class="modal-body d-block" align="center" style="background-color:#DFE6F4;">
            <img src="image/delete12.png" width="100px" height="100px"><br><br><br>
            <h2><b>ลบข้อมูล</b></h2><br>
            <p>คุณต้องการลบข้อมูล <b id="deletename"></b> หรือไม่?? </p><br>
            <form id="form2" name="form2" method="POST">
            <button type="button" class="btn btn-success" data-dismiss="modal"  onclick="deletedataequ()"  >ยืนยัน</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
            </form>
        </div>
      </div>
    </div>
  </div>




<!--แก้ไขข้อมูล-->
<div class="modal" id="edit6" >
    <div class="modal-dialog modal-lg" style="border:4px #FF6D33  solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header d-block">
          <h2 class="modal-title font" style="font-weight: bold;" align="center">แก้ไขข้อมูลอุปกรณ์</h2>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body font">
        <center><img src="image/edit-valid.png" wigth="200px" height="200px" ></center>
        <br>
        <form id="formupdateequ" name="formupdateequ" method="POST">
                                        <label class="font" style="font-size: 20px;">ชื่ออุปกรณ์:</label>
                                        <input hidden type="text" class="form-control" id="equid" name="equid" placeholder="ไอดี(ซ่อน)">
                                        <textarea type="text" class="form-control" id="equname" name="equname" placeholder="ชื่อสินค้า"></textarea>
                                        <br>
                                        <label class="font" style="font-size: 20px;">หน่วยนับ:</label>
                                        <input type="text" class="form-control" id="equnum" name="equnum" placeholder="หน่วยนับ">
                                        <br>
                                        <label class="font" style="font-size: 20px;">ประเภทอุปกรณ์:</label>
                                        <select class="form-control"  id="eq_type_id" name="eq_type_id"style="font-size:16px;">
                                                            <?php
                                                            $strSQL="SELECT * FROM  eq_type ORDER BY eq_type_id ASC" ;
                                                            $result=@$conn->query($strSQL);
                                                            if($result->num_rows>0){
                                                            while($row=$result->fetch_assoc()){
                                                                        ?>
                                                            <option  value="<?php echo trim($row['eq_type_id']);?>">
                                                            <?php echo $row['eq_type_name'];?>
                                                            </option>
                                                            <?php
                                                            }}
                                                            ?>
                                        </select>
                                        <br>
                                        <label class="font" style="font-size:20px">รายละเอียด:</label>
                                         <textarea type="text" class="form-control" id="equdetail" name="equdetail" placeholder="รายละเอียด"></textarea>
                                         <br> 
                                      <!-- Modal footer -->
        <div class="modal-footer d-block" align="center"  style="background-color: #E2FA6E;">
          <button id="mdcolor" class="btn btn-success" type="button" data-dismiss="modal" onclick="acceptupdateequ()">ยืนยันการแก้ไข</button>
          <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
        </div>

        </form>
      </div>
    </div>
        </table>

  </div>





















  
    </body>
</head>


<?php //include connection
include "../setting/config.php ";

?>

<?php //header
@header("content-type:application/json;charset=utf-8");
@header("Access-Control-Allow-Origin: *");
@header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

?>

<?php
$delete_id;
// input
    if($_SERVER["REQUEST_METHOD"]=="POST"){
        $delete_id = $_POST['delete_id'];

    }else{
        $delete_id = "รหัสไม่เข้า";
     
    }

?>

<?php // process
    $strSQL="DELETE FROM eq_type WHERE eq_type_id ='$delete_id' ";

    if ($conn->query($strSQL) === TRUE) {
       
    }

?>

<?php // output
  echo "<meta http-equiv='refresh' content='2 ; URL=../product_type.php'>";
?>

<?php // log
    $ip = $_SERVER['REMOTE_ADDR'];   //ดึงค่า ip address ออกมา
    $date = @date("d/m/Y H:i:s");   //วันที่ส่งข้อมูล 
    $objFopen= @fopen("delete.log","a+");    //ถ้าไม่มีไฟล์ให้สร้าง ถ้ามีให้เขียนทับ
    $str1="\n";
    $str = $date." |-> IP:".$ip." |->คำสั่ง ".@$delete_id."\n".$str1;
   
    @fwrite($objFopen,$str);
    @fclose($objFopen);
?>


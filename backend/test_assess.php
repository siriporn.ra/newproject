<?php
    include "setting/config.php";
?>
<?php
    @session_start();
    @session_cache_expire(30);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>หน้าแรก</title>
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }
        .card{
            background-color: #ffffff;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .bg{
            color:#000000  ;
        }

        .navbar{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:100vh;
    color: #fff;

    position: fixed;
}




.fo{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:30vh;
    color: #fff;
}

.form-group{
    color:#000000 ;
}


.sidenav {
  height: 72%;
  width: 15%;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #DBDDE7;
  overflow-x: hidden;
  padding-top: 20px;
}

.sidenav a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
}

.sidenav a:hover {
  color: #f1f1f1;
}
body{
        background:#FFFFFF;
    }

/***************************************************************** */
    </style>

    
<script>

    function login() {
        document.forms["flogin"].action = "login_process.php";
        document.forms["flogin"].submit();
    }
    function logoutt(){
        document.forms["logout"].action = "logout.php";
        document.forms["logout"].submit();
    }
    function add_row() {
        var table = document.getElementById("myTable");
        count_rows = table.getElementsByTagName("tr").length;

        var row = table.insertRow(count_rows);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);

        cell1.innerHTML = "<input type='text' name='txtA'"+count_rows+"value>";
    }

    function btadd_owner() {
        document.forms["add_owner"].action = "add_owners.php";
        document.forms["add_owner"].submit();
    }
    function del_row(){
        var table = document.getElementById("myTable");
        count_rows = table.getElementsByTagName("tr").length;
        document.getElementById("myTable").deleteRow(count_rows-1);
    }

    
</script>

    <body>

    <div class="container" style="margin-top:5px; margin-left:-10px">
  <div class="row">
  <div class="col-sm-12">
<nav class="navbar navbar-expand-sm navbar-default fixed-top" style="width:100%; height:90px;">
    </a>
    <ul class="navbar-nav mr-auto">
    <?php


$strSQL="SELECT * FROM  store  ORDER BY st_name ASC";
$result=@$conn->query($strSQL);
if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
?>
<a class="navbar-brand" href="/SeniorProject/frontend/index2.php">
        <img  src="../backend/image/<?php echo $row['st_logo'] ?>"width="200px;" height="120%" style="margin-left:-50px; ">   

            <a class="nav-link active">
            <h1 style="margin-top:30px; margin-left:-50px;"><?php echo $row['st_name'] ?></h1>
                
            </a>
</a>
                <?php
    }}
                ?>


    </ul>
    <ul class="navbar-nav">
    <?php //check key
        if (@$_SESSION['key'] == md5(@$_SESSION['ow_email'])) {
            $strSQL="SELECT * FROM owners WHERE ow_email = '".$_SESSION['ow_email']."' ";
            $result = @$conn->query($strSQL);
            while($row = $result->fetch_assoc()){
        //stay in this page if have key from login page
        //เข้าสู่ระบบได้จะแสดงหน้าฟอร์มนี้

        echo " 
       
        <li class=\"nav-item\">
                     <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"><i class=\"far fa-user-circle\"></i> ผู้ใช้ระบบ : " . @$row['ow_name'] ."  สถานะ : ".$_SESSION['ow_status'].  "</a>
                     </li>
         <form id=\"logout\" name=\"logout\">
             <li class=\"nav-item\">
             <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"onclick=\"logoutt()\"cursor:pointer><i class=\"fas fa-sign-out-alt\"></i> ออกจากระบบ</a>
                
             </li>
    </form>";

            }
        } else {
        //go back to login page.php
        echo "
                    
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"cursor:pointer\"><i class=\"fas fa-sign-in-alt\"></i> เข้าสู่ระบบ</a>
                    </li>";
                   
        }
?>
        <!----><div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content" align="center">
                    <!-- Modal Header -->
                    <div class="modal-header d-block"style="background-color: #D9E2F3 ;">
                        <h2 class="form-group modal-title" style="font-weight: bold;">Log In</h2>
                    </div>
                    <form id="flogin" name="flogin" method="POST">
                    <!-- Modal body -->
                    <div class="modal-body"style="background-color: #D9E2F3  ;">
                        <img src="https://img.pngio.com/participant-png-png-image-participant-png-512_512.png" width="150px" height="150px">
                            <div class="form-group col-sm-9">
                                <label for="cus_email" style="font-weight: regular;">Username</label>
                                <input type="text" placeholder="อีเมล" class="form-control" id="ow_email" name="ow_email">
                            </div>
                            <div class="form-group col-sm-9">
                                <label for="cus_pass" style="font-weight: regular;">Password</label>
                                <input type="password" placeholder="รหัสผ่าน" class="form-control" id="ow_pass" name="ow_pass">
                            </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer d-block" align="center"style="background-color: #D9E2F3 ;">
                        <button id="mdcolor" class="btn btn-success" type="button" onclick="login()">เข้าสู่ระบบ</button>
                        <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

       
    </ul>
</nav>
</div>
</div>

<div class="container" style="margin-left:100px;">
<!--logoส่วนที่ 2 -->
    <div class="row">
        <div class="col-sm-4">  
                            <?php

                  

                    $strSQL="SELECT * FROM  store  ORDER BY st_logo ASC";

                    $result=@$conn->query($strSQL);

                    if($result->num_rows>0){
                        while($row=$result->fetch_assoc()){
                    ?>
                             <a >
                             <img  src="image/<?php echo $row['st_logo'] ?>"width="150px;" height="90%" style="margin-top:80px; margin-left:-90px;">   
                             </a>
                                    <?php
                        }}
                                    ?>
                                        </a>
                                 
        </div>
        <div class="col-sm-6" style="margin-top:90px;margin-left:-300px; padding-left:50px;" >
                                <?php

                        

                        $strSQL="SELECT * FROM  store  ORDER BY st_name ASC";

                        $result=@$conn->query($strSQL);

                        if($result->num_rows>0){
                            while($row=$result->fetch_assoc()){
                        ?>
                                    <a>
                                            <h2><?php echo $row['st_name'] ?></h2>
                                            <h6><p>ที่อยู่ :         <?php echo $row['st_add'] ?></p></h6>
                                            <h6><p>เบอร์โทรศัพท์ :  <?php echo $row['st_tel'] ?></p></h6>
                                    </a>
                                        <?php
                            }}
                                        ?>

        </div>
    </div>

    <div class="container" align="center">
    <h3>ใบเสนอราคา
    </div>
    <br>
<div  align="left">
<a> 
  <label for="browser"> ชื่อลูกค้า :</label>
  <input list="browsers" name="nameandid" id="nameandid" onchange="getoutdata(this)">
  <datalist id="browsers" >
  <?php
    include "setting/config2.php";

  $strSQL="SELECT * FROM  owners " ;

  $result=@$conn->query($strSQL);
  if($result->num_rows>0){
    while($row=$result->fetch_assoc()){  ?>
    <option  id="customer_id" value="<?php echo $row['ow_name']."  "."ID:".$row['ow_id'];?>"  customer_email="<?php echo $row['ow_email'];?>" hidden>
    <?php
    }}
?>
  </datalist>

<script>
function checktextbox(){
    if(document.getElementById("nameandid").value==""){

        document.getElementById("customer_tel").value="";
        document.getElementById("customer_address").value="";
    }
}
setInterval(function(){ 
  
    checktextbox()
}, 100);

function getoutdata(){
    var customer_name_and_id= document.getElementById("nameandid").value;
 //   console.log(customer_name_and_id);
    
    var customer_id = customer_name_and_id.split(":")[1];
 //   console.log(customer_id);

        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/getcustomerdetail.php",
                data:JSON.stringify({
                    customer_id:customer_id
                }),
                success: function (response) {
                var json_data = response;
     //           console.log(json_data);

document.getElementById("customer_tel").value=json_data.ow_tel;
document.getElementById("customer_address").value=json_data.ow_add;
                }
            });

}
</script>




<a>ที่อยู่ : <input type="text" id="customer_address" Disabled></a>
<a>เบอร์โทรศัพท์ : <input type="text" id="customer_tel" Disabled></a>

 <form id="add_owner" name="add_owner" method="POST" style="margin-left:800px; margin-top:-35px;">
    <button onclick="btadd_owner()">เพิ่มลูกค้าใหม่</button>
</form>

   
</div>

<div  align="right" style="margin-top:-30px; ">
    <p style="margin-right:-130px;" id="num_quo" name="num_quo">เลขที่ใบเสนอราคา :
    xxxxxxxxxxxxxx
                                <?php
                            //$eqtype_id = sprintf("EQT").date("ymd")."-".rand(0,9);
                            //echo $eqtype_id.$eqtype_name;
                           /* $recgennum = rand(0,99999);
                            $rec_id = sprintf('TWC-%010d', $recgennum);
                            echo $rec_id;// P-0001*/
                                          
                            ?>
                                
                                </p>

    <p style="margin-right:-80px;" id="date">วันที่ :
    
    <?php

        $date_time = date("Y-m-d H:i:s");
       /* $strSQL="INSERT INTO receives (rec_id,rec_date) VALUES ('$rec_id','$date_time')";
        $result=@$conn->query($strSQL); */
            echo $date_time;
        ?>
     </p>



    <span style="margin-right:-80px;">สถานะการประเมิน : 
    <select   id="ptypeid" name="ptypeid"style="font-size:16px;width:130px;" disabled>
               <option  value="รับสินค้า">รับสินค้า</option>
            </select>
    </span>
    <br><br>
                        <p style="margin-right:-80px;">จำนวน : <input type="number"  style=" width:80px;" id="number" name="number">
                        
                        <button onclick="insertdetailreceives()">เพิ่ม</button></p>
</a>

</div>
<script>


        function insertdetailreceives(){
           //ข้อมูลจาก Input box ชื่อลูกค้า
           // รหัสประเภท
           //
            var customer_id = document.getElementById("nameandid").value.split(":")[1];
            var proname= document.getElementById("pro_name").value;
            var dec= document.getElementById("list_dec").value;
            var num= document.getElementById("number").value;
            console.log(num);
            console.log(customer_id);
            console.log(proname);
            console.log(dec);

           $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/getlist_data.php",
                data:JSON.stringify({
                    customer_id:customer_id,
                    proname:proname,
                    dec:dec,
                    num:num
                }),
                success: function (response) {
                var json_data = response;
               console.log(response.result);
               if(response.result=="Success"){
                location.reload();
               }

                }
            });




        }

        function getpro_type(){
            var pro_type = document.getElementById("eq_typeid").value;
 //console.log(pro_type);
        
 $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/getproduct_type.php",
                data:JSON.stringify({
                    pro_type:pro_type
                }),
                success: function (response) {
                var json_data = response;
                var product =" <option >กรุณาเลือกข้อมูล</option>"
               
                $.each(response, function(index) {
                        console.log(response[index].pro_id);
                        console.log(response[index].pro_name);
                        product += "<option value="+"'"+response[index].pro_id+"'"+">"+response[index].pro_name+"</option>";
                    });
                    $('#pro_name').html(product);
                }
            });
 
        }

        function add_rec(obj) {
            
            var list_rec_id = obj.getAttribute("list_rec_idofequ");
            var eq_name = document.getElementById("equ"+list_rec_id).value;
            console.log(eq_name+list_rec_id);

            
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/get_list_receives.php",
                data:JSON.stringify({
                    eq_name:eq_name,
                    list_rec_id:list_rec_id
                }),
                success: function (response) {
                var json_data = response.result;
                    if(json_data=="Success"){
                        location.reload();
                    }
                }
            });
    
         
        }
    
       


</script>
<div style="margin-top:-120px;">
          
                <label for="brow"> ชื่อประเภท :</label>
                <select name="eq_typeid" id="eq_typeid" onchange="getpro_type()">
                <option >กรุณาเลือกข้อมูล</option>
                <?php

                $strSQL="SELECT * FROM  product_type " ;

                $result=@$conn->query($strSQL);
                if($result->num_rows>0){
                    while($row=$result->fetch_assoc()){  ?>
                    <option  id="pro_typeid" value="<?php echo $row['pro_type_id'];?> ">
                    <?php echo $row['pro_type_name'] ?>
                     </option>
                    <?php
                    }}
                ?>
               </select>
            <p>ชื่อสินค้า :
            <select   id="pro_name" name="pro_name"style="font-size:16px;width:20%;" onchange="getproductname()"> 
            <option ></option></select></p>

<script>
function getproductname(){
    console.log("proname");
    var proname = document.getElementById("pro_name").value;
    console.log(proname);
}
</script>




<div class="row">
       
<p>  อาการที่เสีย :  </p> <textarea style="width:20%; hight:20%" id="list_dec" name="list_dec" ></textarea>
        <?php
       /*  $dec=$_POST["list_dec"];
         $strSQL="INSERT INTO receive_list(rec_id,list_rec_decline) VALUES ('$rec_id','$dec')";
         $result=@$conn->query($strSQL);
    echo $dec;*/


        ?>

</div>



</div>

<br><br>
<table class="table table-striped font col-md-12" align="center" >
   <tr class="t" align="center" style="font-size:18px;">
        <th >ลำดับ</th>
        <th  >ประเภทสินค้า</th>
        <th  >สินค้า</th>
        <th  >จำนวน</th>
        <th  >อาการที่เสีย</th>
        <th  >อุปกรณ์ที่ใช้</th>
   </tr>

                                 
      <?php
/*SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate
FROM Orders
INNER JOIN Customers ON Orders.CustomerID=Customers.CustomerID;*/
$strSQL="SELECT * FROM receive_list 
INNER JOIN product ON receive_list.pro_id = product.pro_id 
INNER JOIN product_type ON product.pro_type_id  = product_type.pro_type_id  ";
$result=@$conn->query($strSQL);

      ?>
<?php

if($result->num_rows>0){


    while($row=$result->fetch_assoc()){
      ?>
                    <tr style="font-size:12px;" class="font" >
                        <td><?php echo $row['list_rec_id']; ?></td> 
                        <td><?php echo $row['pro_type_name']; ?></td> 
                        <td><?php echo $row['pro_name']; ?></td> 
                        <td><?php echo $row['list_rec_num']; ?></td> 
                        <td><?php echo $row['list_rec_decline']; ?></td>  
                       
                        <td align="center">
                   

                        <?php
                        $getequipmentlist = "SELECT * FROM add_equipment 
                        INNER JOIN equipment ON add_equipment.eq_id = equipment.eq_id 
                        WHERE list_rec_id = '".$row['list_rec_id']."' ";
                        $resultgetequipmentlist=@$conn->query($getequipmentlist);
                        if($resultgetequipmentlist->num_rows>0){
                        while($rowgetequipmentlist=$resultgetequipmentlist->fetch_assoc()){
                            echo $rowgetequipmentlist['eq_name'].",";
                            }
                        }
                         ?>
                        
                        <select name="equ<?php echo $row['list_rec_id']; ?>" id="equ<?php echo $row['list_rec_id']; ?>" list_rec_idofequ="<?php echo $row['list_rec_id']; ?>"  onchange="add_rec(this)">
                            <?php
                        $getequipment="SELECT * FROM  equipment" ;
                        $resultgetequipment=@$conn->query($getequipment);
                        if($resultgetequipment->num_rows>0){
                            while($rowgetequipment=$resultgetequipment->fetch_assoc()){

                                    $checkequipment="SELECT * FROM add_equipment WHERE eq_id ='".$rowgetequipment['eq_id']."' 
                                    AND list_rec_id = '".$row['list_rec_id']."' ";
                                    $resultcheckequipment=@$conn->query($checkequipment);
                                    if($resultcheckequipment->num_rows>0){
                                     
                                    ?>
                            <?php
                              }else{
                                 ?>
                             <option hidden>กรุณาเลือกอุปกรณ์</option>
                            <option value="<?php echo $rowgetequipment['eq_id'];?> ">
                            <?php echo $rowgetequipment['eq_name'] ?>
                            </option>

                        <?php     }  
                       ?>
                    <?php   }}
                    
                        ?>
                        </select>

                        </td>
                </tr>
      <?php
      
    }}
?>

</table>

<div style="margin-left:100px;"> 
                    <?php
              
                $strSQL="SELECT * FROM  store  ORDER BY st_service_rates ASC";
                $result=@$conn->query($strSQL);
                if($result->num_rows>0){
                    while($row=$result->fetch_assoc()){
                ?>
                    <a>
                        <p>ค่าบริการ : <?php echo $row['st_service_rates'] ?> บาท</p>
                                 
                    </a>
                                <?php
                    }}
                                ?>

                    <p>ค่าอุปกรณ์ <input type="number" style="width:100px;"> บาท</p>
                    <p>รวมเป็นเงิน : บาท <button>บันทึก</button></p>

                    <p>***สามารถรับสินค้าได้ในวันที่ :  <input type="datetime-local" ></p>

    


</div>

    </body>
</head>


<?php

include "setting/config.php";


@session_start();
$alert = @$_SESSION['success'];
unset($_SESSION["success"]);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>จัดการข้อมูลพื้นฐาน</title>

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

<link href="bootstrap/css/animate.css" rel="stylesheet">

<script src="bootstrap/js/jquery-3.4.1.min.js"></script>

<script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>

<script src="bootstrap/js/popper.min"></script>

<script src="bootstrap/js/bootstrap.min.js"></script>

<script src="bootstrap/js/holder.min.js"></script>

<link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
.dropbtn {
  background-color: #FF5D1B ;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  font-size: 14px;

  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #FF7B46;}
</style>



    <script>



    $("#menu-toggle").click(function(e) {
  e.preventDefault();
$("#wrapper").toggleClass("toggled");
});
</script>
<style>
  #wrapper {
    padding-left: 0;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}

#wrapper.toggled {
    padding-left: 250px;
}

#sidebar-wrapper {
    z-index: 1000;
    position: fixed;
    left: 250px;
    width: 0;
    height: 100%;
    margin-left: -250px;
    overflow-y: auto;
    background: #000;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}

#wrapper.toggled #sidebar-wrapper {
    width: 250px;
}

#page-content-wrapper {
    width: 100%;
    position: absolute;
    padding: 10px;
}

#wrapper.toggled #page-content-wrapper {
    position: absolute;
    margin-right: -250px;
}

/* Sidebar Styles */

.sidebar-nav {
    position: absolute;
    top: 0;
    
    margin: 0;
    padding: 0;
    list-style: none;
}

.sidebar-nav li {
    text-indent: 20px;
    line-height: 40px;
}

.sidebar-nav li a {
    display: block;
    text-decoration: none;
    color: #999999;
}

.sidebar-nav li a:hover {
    text-decoration: none;
    color: #fff;
    background: rgba(255,255,255,0.2);
}

.sidebar-nav li a:active,
.sidebar-nav li a:focus {
    text-decoration: none;
}

.sidebar-nav > .sidebar-brand {
    height: 65px;
    font-size: 18px;
    line-height: 60px;
}

.sidebar-nav > .sidebar-brand a {
    color: #999999;
}

.sidebar-nav > .sidebar-brand a:hover {
    color: #fff;
    background: none;
}

@media(min-width:768px) {
    #wrapper {
        padding-left: 250px;
    }

    #wrapper.toggled {
        padding-left: 0;
    }

    #sidebar-wrapper {
        width: 134px;
    }

    #wrapper.toggled #sidebar-wrapper {
        width: 0;
    }

    #page-content-wrapper {
        padding: 20px;
        position: relative;
    }

    #wrapper.toggled #page-content-wrapper {
        position: relative;
        margin-right: 0;
    }
}

::-webkit-scrollbar {
    width: 12px;
}

::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(200,200,200,1);
    border-radius: 10px;
}

::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background-color:#fff;
    -webkit-box-shadow: inset 0 0 6px rgba(90,90,90,0.7);
}



#changecolor{
-webkit-filter: grayscale(100%); /* New WebKit สีโทนเทา*/ 
    -moz-filter: grayscale(100%);
    -ms-filter: grayscale(100%);
    -o-filter: grayscale(100%);
    filter: grayscale(100%);
     filter: gray; /* IE 6-9 */
    
}
a{
    color:white;
    
}
a:hover{
    color:white;
    text-decoration: none;
}

@font-face {
    font-family: 'Opun-Regular.ttf';
    src: url('fonts/Opun-Regular.ttf') format('truetype');
    font-weight: normal;
    font-style: normal;
}
.font {
  font-family: 'Opun-Regular.ttf';
  font-size: 35px;
  
}
.si{
    width:200px;height:80px; /*sidebar จัดการฐานข้อมูล*/
    background-color:#46E7E1;
    
}
.w3-sidebar{ /*สี sidebar จัดการฐานข้อมูล close*/
    background-color:#67C7F8;
}
.te{
    text-align: center;
}

</style>
</head>
<script>
     function w3_open() {
  document.getElementById("mySidebar").style.display = "block";
}
function w3_close() {
  document.getElementById("mySidebar").style.display = "none";
}
</script>
<body  style="background-color: #E4E0E0 ;">


<!-- Page Content -->

<div class="w3-teal" >
   
  <button class="w3-button w3-teal w3-xlarge" onclick="w3_open()" style="font-size:50px;">☰</button>
  <a style="font-size:20px;" align="center" >ข้อมูลพื้นฐานร้านธวัชชัยอิเล็กทรอนิกส์</a>
  <div class="w3-sidebar w3-bar-block w3-border-right" style="display:none " id="mySidebar"style="background-color: #67C7F8;">
  <button onclick="w3_close()" class="w3-bar-item w3-large">Close &times;</button>

  <?php

include "setting/config.php";

$strSQL="SELECT * FROM  information  ORDER BY in_no ASC";

$result=@$conn->query($strSQL);

if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
?>
                <li class="te">
                    <a href="<?php echo $row['in_limkname']  ?>">
                 
                        <img  src="image/<?php echo $row['in_img']  ?>" width="95px" height="85px">
                        <br>
                        <span style="color:#010A0E;"><?php echo $row['in_name']  ?></span>
                    
                    </a>
                </li>
                <?php
    }}
                ?>
            </ul>
        </div>
  
</div>
</div>
<!-- Sidebar -->

<div class="col-md-12" style="background-color: #F9C5CA ;" >

                     
                <h1 align="center" class="font" style="padding-top: 20px; font-size:40px">ข้อมูลสินค้า</h1>

                <div align="right" >
          
            </div>
<br>


<div class="container"style="margin-right:10px;">      

<?php
echo $alert;
?>


  <table class="table table-bordered font" style="margin-left:-70px;">
   
      <tr class="t" align="center" style="font-size:18px;">
        <th >รหัสสินค้า</th>
        <th  >ชื่อสินค้า</th>
        <th  >ราคาประเมิน</th>
        <th  >ยี่ห้อ</th>
        <th  >รุ่น</th>
        <th  >ประเภท</th>
        <th  width="10%">
        <label ><b><button style="background-color: #2A69F3 "><a href="add_product.php">
        เพิ่ม</a> </button></b></label>
        </th>
      </tr>
      
      <?php
    $strSQL="SELECT * FROM product INNER JOIN product_type ON product_type.pro_type_id = product.pro_type_id" ;

    $result=@$conn->query($strSQL);

      ?>

  
<?php

if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
      ?>

                    <tr style="font-size:12px;" class="font" >
                        <td><?php echo $row['pro_id']; ?></td> 
                        <td><?php echo $row['pro_name']; ?></td> 
                        <td><?php echo $row['pro_price']; ?></td>
                        <td><?php echo $row['pro_brand']; ?></td> 
                        <td><?php echo $row['pro_generation']; ?></td> 
                        <td><?php echo $row['pro_type_name']; ?></td>  
                        <td align="center">

    <a data-toggle="modal" data-target="#edit" 
                         pid="<?php echo $row['pro_id'];  ?>"
                         pname="<?php echo $row['pro_name'];  ?>"
                         pprice="<?php echo $row['pro_price'];  ?>"
                         pbrand="<?php echo $row['pro_brand'];  ?>"
                         pgen="<?php echo $row['pro_generation'];  ?>"
                         typeid="<?php echo $row['pro_type_id'];  ?>"
                         onclick="editstroe(this)" style="cursor:pointer;"><button style="background-color: #5FF891">แก้ไข</button></a>
    <a  data-toggle="modal" data-target="#myModal" 
                        delete_id="<?php echo $row['pro_id']; ?>"
                        delete_name="<?php echo " ชื่อ : ".$row['pro_name']; ?>" 
                        onclick="sdeletedata(this)"style="cursor:pointer;" ><button style="background-color: #FD5244 ">ลบ</button></a>

</div>                                            
                      </td>
                </tr>
      <?php
      
    }}
?>
      </div>
</div>
</div>    

                                      <!-- Delete ลบข้อมูล -->
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg" style="border:4px #FF6D33  solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
      <div class="modal-content">
        
        <!-- Modal body -->
        <div class="modal-body d-block" align="center" style="background-color:#DFE6F4;">
            <img src="image/delete12.png" width="100px" height="100px"><br><br><br>
            <h2><b>ลบข้อมูล</b></h2><br>
            <p>คุณต้องการลบข้อมูล <b id="deletename"></b> หรือไม่?? </p><br>
            <form id="form2" name="form2" method="POST">
            <button type="button" class="btn btn-success" data-dismiss="modal"  onclick="deletedata()"  >ยืนยัน</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
            </form>
        </div>
      </div>
    </div>
  </div>




<!--แก้ไขข้อมูล-->
<div class="modal" id="edit" >
    <div class="modal-dialog modal-lg" style="border:4px #FF6D33  solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header d-block">
          <h2 class="modal-title font" style="font-weight: bold;" align="center">แก้ไขข้อมูลสินค้า</h2>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body font">
        <center><img src="image/edit-valid.png" wigth="200px" height="200px" ></center>
        <br>
        <form id="formupdateuser" name="formupdateuser" method="POST">

                                        <label class="font" style="font-size: 20px;">ชื่อสินค้า:</label>
                                        <textarea type="text" class="form-control" id="pname" name="pname" placeholder="ชื่อสินค้า"></textarea>
                                        <br>
                                        <label class="font" style="font-size: 20px;">ราคาประเมิน:</label>
                                        <input  type="text" class="form-control" id="pprice" name="pprice" placeholder="ราคาประเมิน">
                                        <br>
                                        <label class="font" style="font-size: 20px;">ยี่ห้อ:</label>
                                        <input type="text" class="form-control" id="pbrand" name="pbrand" placeholder="ยี่ห้อ">
                                        <br>
                                        <label class="font" style="font-size: 20px;">รุ่น:</label>
                                        <textarea type="text" class="form-control" id="pgen" name="pgen" placeholder="รุ่น"></textarea>
                                        <br>
                                        <label class="font" style="font-size: 20px;">ประเภทสินค้า:</label>
                                        <select class="form-control"  id="ptypeid" name="ptypeid"style="font-size:16px;">
                                                            <?php
                                                            $strSQL="SELECT * FROM  product_type ORDER BY pro_type_id ASC" ;
                                                            $result=@$conn->query($strSQL);
                                                            if($result->num_rows>0){
                                                            while($row=$result->fetch_assoc()){
                                                                        ?>
                                                            <option  value="<?php echo trim($row['pro_type_id']);?>">
                                                            <?php echo $row['pro_type_name'];?>
                                                            </option>
                                                            <?php
                                                            }}
                                                            ?>
                                        </select>
                                        <br>
                                       
                                      <!-- Modal footer -->
        <div class="modal-footer d-block" align="center"  style="background-color: #E2FA6E;">
          <button id="mdcolor" class="btn btn-success" type="button" data-dismiss="modal" onclick="acceptupdate()">ยืนยันการแก้ไข</button>
          <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
        </div>
      </div>
    </div>
  </div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />



<script>

function acceptupdate(){
document.forms["formupdateuser"].action="updateproduct.php";
document.forms["formupdateuser"].submit();

  }

function editstroe(obj){
    var p_id =obj.getAttribute("pid");
    var p_name =obj.getAttribute("pname");
    var p_price =obj.getAttribute("pprice");
    var p_brand =obj.getAttribute("pbrand");
    var p_gen =obj.getAttribute("pgen");
    var p_type_id =obj.getAttribute("typeid");


    document.getElementById("pid").value = p_id;
    document.getElementById("pname").value = p_name;
    document.getElementById("pprice").value = p_price;
    document.getElementById("pbrand").value = p_brand;
    document.getElementById("pgen").value = p_gen;
    document.getElementById("ptypeid").value = p_type_id;

}






/*<!-- ลบข้อมูล -->*/

var globalid;
function sdeletedata(obj){
   
    var protid =obj.getAttribute("delete_id");
    var protname =obj.getAttribute("delete_name");
    globalid=protid

    document.getElementById("deletename").innerHTML = protname;
    document.getElementById("deleteid").innerHTML = protid;
}

function deletedata(){
  //  alert(globalid)
//alert(globalid);
    $.ajax({ //กล่องๆนึงรวมข้อมูลไว้
                type:"POST",
                async :false, //ทำให้ทำงานเรียงตามลำดับ
                url:"webservice/deleteproduct.php",
                data:{
                    delete_id:globalid
                }
            });
            location.reload();
}
</script>






<?php
    include "setting/config.php";
    @session_start();
    @session_cache_expire(30);

    $rec_idfromedit = $_GET["formedit"];
    $datereveives = "";
    $recstatus = "";
    $ow_data  = "";
    $stredit_list="SELECT * FROM  receives 
    INNER JOIN owners ON receives.ow_id = owners.ow_id
    WHERE rec_id='".$rec_idfromedit."' ";
    $resultstredit_list=@$conn->query($stredit_list);
    if($resultstredit_list->num_rows>0){
        while($rowstredit_list=$resultstredit_list->fetch_assoc()){
            $ow_data=$rowstredit_list['ow_name'];
            $datereveives = $rowstredit_list['rec_date'];
            $recstatus = $rowstredit_list['rec_status'];
        }}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>แก้ไขบิลรายการรับและประเมินราคา</title>
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <style>
        @font-face {
            font-family: 'ANGSA.ttf';
            src: url('fonts/ANGSA.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }
        .card{
            background-color: #ffffff;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .bg{
            color:#000000  ;
        }

        .navbar{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:100vh;
    color: #fff;

    position: fixed;
}


/***************************************************************** */
    </style>

    
<script>

    function login() {
        document.forms["flogin"].action = "login_process.php";
        document.forms["flogin"].submit();
    }
    function logoutt(){
        document.forms["logout"].action = "logout.php";
        document.forms["logout"].submit();
    }
    function add_row() {
        var table = document.getElementById("myTable");
        count_rows = table.getElementsByTagName("tr").length;

        var row = table.insertRow(count_rows);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);

        cell1.innerHTML = "<input type='text' name='txtA'"+count_rows+"value>";
    }

    function btadd_owner() {
        document.forms["add_owner"].action = "add_owners.php";
        document.forms["add_owner"].submit();
    }
    function del_row(){
        var table = document.getElementById("myTable");
        count_rows = table.getElementsByTagName("tr").length;
        document.getElementById("myTable").deleteRow(count_rows-1);
    }
    function all_items() {
        document.forms["items"].action = "show_list_rec.php";
        document.forms["items"].submit();
    }
    
</script>
<!--////////////////////////////////////////////แก้ไขสถานะ///////////////////////////////////////////////////////////////////-->
<script>
    function updaterecstatus(){
        var rec_status = document.getElementById("ptypeid").value;
        var rec_id = "<?php echo $rec_idfromedit;?>";
        console.log(rec_status+rec_id);
                $.ajax({

                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/edit_rec_status.php",
                data:JSON.stringify({
                    //eq_price:eq_price,
                    rec_status:rec_status,
                    rec_id:rec_id
                }),
                success: function (response) {
                var json_data = response.result;
                if(json_data=="Success"){
                alert("แก้ไขสถานะสำเร็จ");
                    location.reload();
                }
                }
                });
    }
    </script>
  
<!--เพิ่ม -->


<script>


        function insertdetailreceives(){
           //ข้อมูลจาก Input box ชื่อลูกค้า
           // รหัสประเภท
           //
            var proname= document.getElementById("pro_name").value;
            var dec= document.getElementById("list_dec").value;
            var num= document.getElementById("number").value;
            var list_rec_price= document.getElementById("list_rec_price").value;
            console.log(num);
            console.log(customer_id);
            console.log(proname);
            console.log(dec);
            console.log(list_rec_price);

           $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/getlist_data.php",
                data:JSON.stringify({
                    proname:proname,
                    dec:dec,
                    num:num,
                    list_rec_price:list_rec_price
                }),
                success: function (response) {
                var json_data = response;
               console.log(response.result);
               if(response.result=="Success"){
                location.reload(); 
               }

                }
            });

//เช็คค่าว่าง

    
        }
//เลือกสินค้าและประเภทสินค้า
        function getpro_type(){
            var pro_type = document.getElementById("eq_typeid").value;
 //console.log(pro_type);
        
 $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/getproduct_type.php",
                data:JSON.stringify({
                    pro_type:pro_type
                }),
                success: function (response) {
                var json_data = response;
                var product =" <option hidden>กรุณาเลือกข้อมูล</option>"
               
                $.each(response, function(index) {
                        console.log(response[index].pro_id);
                        console.log(response[index].pro_name);
                        product += "<option value="+"'"+response[index].pro_id+"'"+">"+response[index].pro_name+"</option>";
                    });
                    $('#pro_name').html(product);
                }
            });
 
        }
//เลือกอุปกรณ์ที่ใช้
        function add_rec(obj) {
            
            var list_rec_id = obj.getAttribute("list_rec_idofequ");
            var eq_name = document.getElementById("equ"+list_rec_id).value;
            console.log(eq_name+list_rec_id);

            
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/get_list_receives.php",
                data:JSON.stringify({
                    eq_name:eq_name,
                    list_rec_id:list_rec_id
                }),
                success: function (response) {
                var json_data = response.result;
                    if(json_data=="Success"){
                        location.reload();
                    }
                }
            });
    
         
        }
    
</script>

<!--     -->
<script>



//เลือกสินค้าและประเภทสินค้า
        function getpro_type(){
            var pro_type = document.getElementById("eq_typeid").value;
 //console.log(pro_type);
        
 $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/getproduct_type.php",
                data:JSON.stringify({
                    pro_type:pro_type
                }),
                success: function (response) {
                var json_data = response;
                var product =" <option hidden>กรุณาเลือกข้อมูล</option>"
               
                $.each(response, function(index) {
                        console.log(response[index].pro_id);
                        console.log(response[index].pro_name);
                        product += "<option value="+"'"+response[index].pro_id+"'"+">"+response[index].pro_name+"</option>";
                    });
                    $('#pro_name').html(product);
                }
            });
 
        }
//เลือกอุปกรณ์ที่ใช้
        function add_rec(obj) {
            
            var list_rec_id = obj.getAttribute("list_rec_idofequ");
            var eq_name = document.getElementById("equ"+list_rec_id).value;
            console.log(eq_name+list_rec_id);

            
            $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/get_list_receives.php",
                data:JSON.stringify({
                    eq_name:eq_name,
                    list_rec_id:list_rec_id
                }),
                success: function (response) {
                var json_data = response.result;
                    if(json_data=="Success"){
                        location.reload();
                    }
                }
            });
    
         
        }
    

</script>
<!--ลบข้อมูลรายการ-->
<script>
//ลบข้อมูลรายการรับ
var globalid;
function deletedata_receive_list(obj){
   
    var protid =obj.getAttribute("delete_id");
    var protname =obj.getAttribute("delete_name");
    globalid=protid

    document.getElementById("deletename").innerHTML = protname;
    document.getElementById("deleteid").innerHTML = protid;
}

            function deletedata_receive_list_detail(obj) {
                $.ajax({ //กล่องๆนึงรวมข้อมูลไว้
                type:"POST",
                async :false, //ทำให้ทำงานเรียงตามลำดับ
                url:"webservice/delete_list_recevice.php",
                data:{
                    delete_id:globalid
                }
            });
            location.reload();
}
          

</script>

<div class="modal" id="myModal1">
  <div class="modal-dialog modal-lg" style="border:4px #F2D1D1   solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
      <div class="modal-content">
        
        <!-- Modal body -->
        <div class="modal-body d-block" align="center" style="background-color:#DFE6F4;">
            <img src="image/delete12.png" width="100px" height="100px"><br><br><br>
            <h2><b>ลบข้อมูล</b></h2><br>
            <p>คุณต้องการลบข้อมูล <b id="deletename"></b> หรือไม่?? </p><br>
            <form id="form2" name="form2" method="POST">
            <button type="button" class="btn btn-success" data-dismiss="modal" onclick="deletedata_receive_list_detail()"  >ยืนยัน</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
            </form>
        </div>
      </div>
    </div>
  </div>         

<!--*******************************************************แสดงค่ารายการ*****************************************************************-->
<script>
            function get_receive_list_detail(obj) {
                var list_rec_id = obj.getAttribute("list_rec_id");
                console.log(list_rec_id);
                $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/get_receive_list_detail.php",
                data:JSON.stringify({
                    list_rec_id:list_rec_id
                }),
                success: function (response) {
                var json_data = response;
                console.log(json_data.list_rec_id);
                document.getElementById("protype_id").value=json_data.pro_type_id;
                document.getElementById("product1").value=json_data.pro_id;
                document.getElementById("num_ber").value=json_data.list_rec_num;
                document.getElementById("price_rec").value=json_data.list_rec_price;
                document.getElementById("rec_dac").value=json_data.list_rec_decline;
                document.getElementById("list_rec_id").value=json_data.list_rec_id;      
                }
            });

                
            }
         
            function acceptupdate_list_recevice(){
                document.forms["update_list_recvice"].action="update_list_recevice.php";
                document.forms["update_list_recvice"].submit();

            }


         </script>

         <script>
    function get_list_proname(){

    var producttype1 = document.getElementById("protype_id").value;

    $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/getproduct_type.php",
                data:JSON.stringify({
                    pro_type:producttype1
                }),
                success: function (response) {
                var json_data = response;
                var product =" <option hidden>กรุณาเลือกข้อมูล</option>"
               
                $.each(response, function(index) {
                        console.log(response[index].pro_id);
                        console.log(response[index].pro_name);
                        product += "<option value="+"'"+response[index].pro_id+"'"+">"+response[index].pro_name+"</option>";
                    });
                    $('#product1').html(product);
                }
            });

}
    </script>
<!--*/*/*/*/*****************************************************************************/*//*/**-->
<script>
function checktextbox(){
    if(document.getElementById("nameandid").value==""){

        document.getElementById("customer_tel").value="";
        document.getElementById("customer_address").value="";
    }
}
setInterval(function(){ 
    getoutdata()
    checktextbox()
}, 100);

function getoutdata(){
    var customer_name_and_id= document.getElementById("nameandid").value;
 //   console.log(customer_name_and_id);
    
    var customer_id = customer_name_and_id.split(":")[1];
 //   console.log(customer_id);

        $.ajax({
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/getcustomerdetail.php",
                data:JSON.stringify({
                    customer_id:customer_id
                }),
                success: function (response) {
                var json_data = response;
     //           console.log(json_data);

document.getElementById("customer_tel").value=json_data.ow_tel;
document.getElementById("customer_address").value=json_data.ow_add;
                }
            });

}
</script>

<!--------------------------------------------------------------------------------คำนวน-------------------------------------------------------------------------->

<script>

setInterval(function(){
    
     var list_total = parseFloat(document.getElementById("list_total").innerHTML.trim());
     console.log(list_total);
     var service_price = parseFloat(document.getElementById("st_service_rates").innerHTML.trim());
     console.log(service_price);
     var total=list_total+service_price;
     console.log(total);
     document.getElementById("sumtotal").innerHTML=total;

}, 50);

     function save_list_rec() {
      //  var rec_id  = document.getElementById("num_quo").value;
      //  var date = document.getElementById("date").value;
      //  var rec_status = document.getElementById("ptypeid").value;
        var ow_id  = document.getElementById("nameandid").value;
        //var eq_price = parseFloat( document.getElementById("total").innerHTML.trim());
        var ass_date  = document.getElementById("rec_date").value;
       // console.log(eq_price);
        var ow_id = ow_id.split(":")[1];
        console.log(ow_id);
        console.log(ass_date);
       
       $.ajax({

                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                async : false,
                url: "webservice/save_list.php",
                data:JSON.stringify({
                    //eq_price:eq_price,
                    ow_id:ow_id,
                    ass_date:ass_date
                    
                }),
                success: function (response) {
                var json_data = response.result;
                if(json_data=="Success"){
                   alert("บันทึกสำเร็จ");
                    location.reload();
                }
                }
            });
   
}


</script>
<!--------------------------------------------------------------------------------คำนวน-------------------------------------------------------------------------->

<!-- ซ่อน body -->
    <body class="container col-lg-10" align="center" style="background-color:#F0FFFF;" hidden>
    <div  style="margin-top:15px;">
  <div class="row">
  <div class="col-md-12">

<div  style="margin-left:100px;">
<!--logoส่วนที่ 2 -->

    <div class="row">
        <div class="col-sm-4">  
                            <?php

                  

                    $strSQL="SELECT * FROM  store  ORDER BY st_logo ASC";

                    $result=@$conn->query($strSQL);

                    if($result->num_rows>0){
                        while($row=$result->fetch_assoc()){
                    ?>
                             <a >
                             <img  src="image/<?php echo $row['st_logo'] ?>"width="260px;" height="90%" style="margin-top:65px; margin-left:-90px;">   
                             </a>
                                    <?php
                        }}
                                    ?>
                                        </a>
                                 
        </div>
 <?php

require_once __DIR__ . '\..\vendor\autoload.php';

$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];

$mpdf = new \Mpdf\Mpdf([
    'fontDir' => array_merge($fontDirs, [
        __DIR__ . '/tmp',
    ]),
    'fontdata' => $fontData + [
       "angsana" => [
					'R' => "ANGSA.ttf",
					'B' => "AngsanaNewBoldItalic.ttf",

        ]
    ],
    'default_font' => 'angsana'
]);



ob_start();
?>       
        <div class="col-sm-3">
                                <?php
                        $strSQL="SELECT * FROM  store  ORDER BY st_name ASC";
                        $result=@$conn->query($strSQL);
                        if($result->num_rows>0){
                            while($row=$result->fetch_assoc()){
                        ?>
                                            <div style="margin-top:-100px;"> <h1><?php echo $row['st_name'] ?></h1></div>
                                            <div style="margin-top:-40px;"> <h3>ที่อยู่ :<?php echo $row['st_add'] ?></h3></div>
                                            <div style="margin-top:-30px;"><h3 >เบอร์โทรศัพท์ :  <?php echo $row['st_tel'] ?></h3></div>
                                            
                            
                                        <?php
                            }}
                                        ?>

        </div>
    </div>

    <h2 align="center">ใบเสนอราคา</h2>
 
    <br>


<div  style="margin-top:-30px;">
<div  style="margin-top:-2px;">
    <p  style="font-size:18px;" align="right" id="num_quo" name="num_quo">เลขที่ใบเสนอราคา :
                                <?php
                       
                         echo $rec_idfromedit;
                                          
                            ?>

                                </p>
</div>
<div  style="margin-top:-40px;">
    <p style="font-size:18px;" align="right" id="date" name="date">วันที่รับ :
    
    <?php
            echo $datereveives;
        ?>
     </p>
  </div>                    
</div>

<script>
function getproductname(){
    console.log("proname");
    var proname = document.getElementById("pro_name").value;
    console.log(proname);
    
}
</script>

<div class="row">
<textarea style=" width:50%; height:20%" id="list_dec" name="list_dec" disabled></textarea>
        <?php
       /*  $dec=$_POST["list_dec"];
         $strSQL="INSERT INTO receive_list(rec_id,list_rec_decline) VALUES ('$rec_id','$dec')";
         $result=@$conn->query($strSQL);
    echo $dec;*/

        ?>
</div>

<div style="margin-top:-90px; " >  

<?php
              
              $strSQLow="SELECT * FROM  receives
              INNER JOIN owners ON receives.ow_id=owners.ow_id
                WHERE rec_id='".$rec_idfromedit."' ";
              $result1=@$conn->query($strSQLow);
              if($result1->num_rows>0){
                  while($row=$result1->fetch_assoc()){
              ?>
            
    <p style="font-size:18px; "> ชื่อลูกค้า : คุณ<?php echo $row['ow_name'] ?> </p>
    <span style="font-size:18px; ">  &nbsp; &nbsp;ที่อยู่ : <?php echo $row['ow_add'] ?> </span>
    <span style="font-size:18px;  " <?php echo $ow_data; ?>> &nbsp; &nbsp; เบอร์โทร : <?php echo $row['ow_tel'] ?> </span>

                              <?php
                  }}
                              ?>
  
   
</div>
<p >______________________________________________________________________________________________________________________</p>
<div  style="margin-top:-10px;">
<table class="table table-info">
   <tr  class="table table-primary " style="font-size:36px; ">

         <th style="font-size:20px; padding-right:60px;">ลำดับ</th>
         <th style="font-size:20px; padding-right:70px;">สินค้า</th>
         <th style="font-size:20px; padding-right:70px;">จำนวน</th>
         <th style="font-size:20px; padding-right:70px;" >ราคาประเมิน</th>
         <th style="font-size:20px; padding-right:70px;" >อาการที่เสีย</th>

 
   </tr>
   <p>______________________________________________________________________________________________________________________</p>
      <?php
$no=1;      
$strSQL="SELECT * FROM receive_list 
INNER JOIN product ON receive_list.pro_id = product.pro_id 
INNER JOIN product_type ON product.pro_type_id  = product_type.pro_type_id WHERE rec_id='".$rec_idfromedit."' ";
$result=@$conn->query($strSQL);
$totalprice=0;
   

      ?>
    <?php
        if($result->num_rows>0){
            while($row=$result->fetch_assoc()){
            $totalprice = $totalprice + $row['list_rec_price'];
      ?>

                    <tr style="font-size:16px;" class="font" align="center">
            <p>______________________________________________________________________________________________________________________</p>        
                        <td align="center" style="padding-right:60px; font-size:18px;"><?php echo $no; ?></td>
                        <td align="center" style="padding-right:70px; font-size:18px;"><?php echo $row['pro_name']; ?></td> 
                        <td align="center" style="padding-right:70px; font-size:18px;"><?php echo $row['list_rec_num']; ?></td>
                        <td align="right"  style="padding-right:70px; font-size:18px;"><?php echo number_format($row['list_rec_price'],2);  ?></td>   
                        <td align="center" style="padding-right:70px; font-size:18px;"><?php echo $row['list_rec_decline']; ?></td>  
                 
                 

     
      <?php
      
      $no++;}}
?>

</table>
<p style="margin-top:-10px;">______________________________________________________________________________________________________________________</p>
</div>
        <p style="font-size:18px;"  align="right">ราคารวมรายการสินค้า : <span id="list_total" name="list_total"><?php echo  number_format($totalprice,2); ?></span>  บาท</p>
  



<!--คำนวนราคา-->

<div style="margin-top:-60px;"> 

                    <?php
              
                $strSQL="SELECT * FROM  store  ORDER BY st_service_rates ASC";
                $result=@$conn->query($strSQL);
                if($result->num_rows>0){
                    while($row=$result->fetch_assoc()){
                ?>
                    
            <div style="margin-top:-40px;">
                        <p  align="right" style="font-size:18px; ">อัตราค่าบริการ : 
                        <span id="st_service_rates" name="st_service_rates">
                        <?php
                        $st_service_rates=$row['st_service_rates'];
                        echo $row['st_service_rates']; ?> 
                        </span>
                        บาท   
                    </p>
             </div>
                                <?php
                    }}
                                ?>
                     <script>

                     function sumtotal(){
                         var list_rec_price=document.getElementById("list_rec_price").value;
                         var list_rec_price=parseInt(list_rec_price);
                         var st_service_rates=document.getElementById("st_service_rates").innerHTML;
                         var st_service_rates = parseInt(st_service_rates.trim());
                         document.getElementById("total").innerHTML=st_service_rates+list_rec_price;

                     }
                     </script>
                      <div style="margin-top:-40px;">
                    <p  align="right" style="font-size:18px; ">รวมเป็นเงิน : <?php echo  number_format ($totalprice+$st_service_rates,2);  ?><span id="sumtotal" name="sumtotal" >
                    </span> บาท </div>
                    <button id="save_list_receives" name="save_list_receives" onclick="save_list_rec()"disabled></button>

                        
                    <?php
              
              $strSQL="SELECT * FROM  assign WHERE rec_id='".$rec_idfromedit."' ";
              $result=@$conn->query($strSQL);
              if($result->num_rows>0){
                  while($row=$result->fetch_assoc()){
              ?>
               <div style="margin-top:-40px;">
                <p style="font-size:18px;  margin-left:420px;">***สามารถรับสินค้าได้ในวันที่ : <?php echo $row['ass_date'] ?> </p>
               </div>
                              <?php
                  }}
                              ?>

         

          
</div>
   <div>
         <p  style="font-size:18px;  margin-left:500px;">ลงชื่อ........................เจ้าของกิจการ</p>
         <p  style="font-size:18px;  margin-left:510px;">ลงชื่อ........................ลูกค้า</p>


   </div>

    </body>
    <?php
    $html=ob_get_contents();
    $mpdf->WriteHTML($html);
    $mpdf->Output("MyReport.pdf");
    ob_end_flush();
    ?> 


    <script>
        //gen report every 100 millisec
        setInterval(function(){ 
           location.replace("MyReport.pdf");
        }, 100);
        // MyReport.pdf
    </script>
</head>
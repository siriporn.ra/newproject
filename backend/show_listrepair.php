<?php
    include "setting/config.php";
?>
<?php
    @session_start();
    @session_cache_expire(30);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>บันทึกรายการซ่อมและแจ้งรับสินค้า</title>
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="bootstrap/css/animate.css" rel="stylesheet">
    <script src="bootstrap/js/jquery-3.4.1.min.js"></script>
    <script src="bootstrap/js/jquery-3.4.1.slim.min.js"></script>
    <script src="bootstrap/js/popper.min"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/js/holder.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Chonburi&display=swap" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/all.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }
        .card{
            background-color: #ffffff;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .bg{
            color:#000000  ;
        }

        .navbar{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:100vh;
    color: #fff;

    position: fixed;
}




.fo{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:30vh;
    color: #fff;
}

.form-group{
    color:#000000 ;
}

body{
        background:#FFFFFF;
    }

/***************************************************************** */
    </style>

    
<script>

    function login() {
        document.forms["flogin"].action = "login_process.php";
        document.forms["flogin"].submit();
    }
    function logoutt(){
        document.forms["logout"].action = "logout.php";
        document.forms["logout"].submit();
    }
    function add_row() {
        var table = document.getElementById("myTable");
        count_rows = table.getElementsByTagName("tr").length;

        var row = table.insertRow(count_rows);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);

        cell1.innerHTML = "<input type='text' name='txtA'"+count_rows+"value>";
    }

    function btadd_owner() {
        document.forms["add_owner"].action = "add_owners.php";
        document.forms["add_owner"].submit();
    }
    function del_row(){
        var table = document.getElementById("myTable");
        count_rows = table.getElementsByTagName("tr").length;
        document.getElementById("myTable").deleteRow(count_rows-1);
    }
    function all_items() {
        document.forms["items"].action = "show_list_rec.php";
        document.forms["items"].submit();
    }
    
</script>

    <body class="container col-lg-10" align="center" style="background-color:#F0FFFF;">

    <div  style="margin-top:15px;">
  <div class="row">
  <div class="col-md-12">
<nav class="navbar navbar-expand-sm navbar-default fixed-top" style="width:100%; height:90px;">
    </a>
    <ul class="navbar-nav mr-auto">
    <?php


$strSQL="SELECT * FROM  store  ORDER BY st_name ASC";
$result=@$conn->query($strSQL);
if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
?>
<a class="navbar-brand" href="/SeniorProject/frontend/index2.php">
        <img  src="../backend/image/<?php echo $row['st_logo'] ?>"width="200px;" height="120%" style="margin-left:-50px; ">   

            <a class="nav-link active">
            <h1 style="margin-top:30px; margin-left:-50px;"><?php echo $row['st_name'] ?></h1>
                
            </a>
</a>
                <?php
    }}
                ?>


    </ul>
    <ul class="navbar-nav">
    <?php //check key
        if (@$_SESSION['key'] == md5(@$_SESSION['ow_email'])) {
            $strSQL="SELECT * FROM owners WHERE ow_email = '".$_SESSION['ow_email']."' ";
            $result = @$conn->query($strSQL);
            while($row = $result->fetch_assoc()){
        //stay in this page if have key from login page
        //เข้าสู่ระบบได้จะแสดงหน้าฟอร์มนี้

        echo " 
       
        <li class=\"nav-item\">
                     <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"><i class=\"far fa-user-circle\"></i> ผู้ใช้ระบบ : " . @$row['ow_name'] ."  สถานะ : ".$_SESSION['ow_status'].  "</a>
                     </li>
         <form id=\"logout\" name=\"logout\">
             <li class=\"nav-item\">
             <a style=\"color: #FFFFFF  !important;,font-size: 14px !important;\" class=\"nav-link\"onclick=\"logoutt()\"cursor:pointer><i class=\"fas fa-sign-out-alt\"></i> ออกจากระบบ</a>
                
             </li>
    </form>";

            }
        } else {
        //go back to login page.php
        echo "
                    
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#myModal\" style=\"cursor:pointer\"><i class=\"fas fa-sign-in-alt\"></i> เข้าสู่ระบบ</a>
                    </li>";
                   
        }
?>
        <!----><div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content" align="center">
                    <!-- Modal Header -->
                    <div class="modal-header d-block"style="background-color: #D9E2F3 ;">
                        <h2 class="form-group modal-title" style="font-weight: bold;">Log In</h2>
                    </div>
                    <form id="flogin" name="flogin" method="POST">
                    <!-- Modal body -->
                    <div class="modal-body"style="background-color: #D9E2F3  ;">
                        <img src="https://img.pngio.com/participant-png-png-image-participant-png-512_512.png" width="150px" height="150px">
                            <div class="form-group col-sm-9">
                                <label for="cus_email" style="font-weight: regular;">Username</label>
                                <input type="text" placeholder="อีเมล" class="form-control" id="ow_email" name="ow_email">
                            </div>
                            <div class="form-group col-sm-9">
                                <label for="cus_pass" style="font-weight: regular;">Password</label>
                                <input type="password" placeholder="รหัสผ่าน" class="form-control" id="ow_pass" name="ow_pass">
                            </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer d-block" align="center"style="background-color: #D9E2F3 ;">
                        <button id="mdcolor" class="btn btn-success" type="button" onclick="login()">เข้าสู่ระบบ</button>
                        <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

       
    </ul>
</nav>
</div>
</div>

<div style="margin-left:-250px; margin-right:50px;">
<div class="widget stacked widget-table action-table" style="margin-top:100px;">
    				
				<div>
					<h3 style="margin-left:700px;">บันทึกรายการซ่อมและแจ้งรับสินค้า</h3><br>
				</div> <!-- /widget-header -->
				
				<div class="widget-content">
               
					<table  class="table table-striped table-bordered" style="margin-left:150px">
						<thead >
							<tr align="center">
								<th>ลำดับ</th>
                                <th>เลขที่ใบเสนอราคา</th>
                                <th>ชื่อลูกค้า</th>
                                <th>วันที่รับสินค้า</th>
                                <th>สถานะ</th>
                                <th class="td-actions"></th>
                                <form name="formeditrec1" id="formeditrec1" method="post">
                                <th style="cursor:pointer;"><img src="image/add11.png" wigth="35px" height="35px"  onclick="open_list_rec(this)"></th>
                                 </form>
							</tr>
						</thead>
						<tbody>
							

                            <?php


//$strSQL="SELECT * FROM  receive_list ";
//$strSQL="SELECT * FROM  receive_list INNER JOIN receives ON receive_list.rec_id=receives.rec_id ";
$no=1;
$strSQL="SELECT * FROM receives 
INNER JOIN owners ON receives.ow_id = owners.ow_id
WHERE rec_status = '0' "; 
$result=@$conn->query($strSQL);
if($result->num_rows>0){
    while($row=$result->fetch_assoc()){
?>
            <a >
            <tr>     
            <td><?php echo $no; ?></td>
            <td><?php echo $row['rec_id'] ?></td>
            <td <?php echo $row['ow_id'] ?>><?php echo $row['ow_name'] ?></td>
            <td><?php echo $row['rec_date'] ?></td>
            
            <td>
            <?php 
            
           if($row['rec_status']=="0"){
            echo "รับสินค้า";
           }else if($row['rec_status']=="1"){
            echo "แจ้งรับสินค้า";
           }else if($row['rec_status']=="2"){
            echo "ส่งคืน";
           }
            ?>
            
            <td class="td-actions"align="center" >

                                    <form name="formeditrec1" id="formeditrec1" method="post">
                                    <button type="button" class="btn btn-warning"
                                    rec_id="<?php echo $row['rec_id'] ?>";
                                    onclick="listrepair(this)"
                                   >แสดงรายการ</button>
                                    </form>

                                    <td align="center">
                                    <form  name="formeditrec1" id="formeditrec1" method="post">
                                    <button type="button" class="btn btn-success" 
                                    rec_id="<?php echo $row['rec_id'] ?>";
                                    onclick="printreceives(this)"
                                    target="_blank">พิมพ์</button>	
                                    </form>									
									</td>
								</td>
                                                   
							
            </tr>
            
          

            </a>
                <?php
      $no++;}}
                ?>
							     <!-- EDIT -->
                                 <script>
                                        function listrepair(obj){
                                            var rec_id = obj.getAttribute("rec_id");
                                            console.log(rec_id);

                                            document.forms["formeditrec1"].action="list_repair.php?formedit="+rec_id;
                                            document.forms["formeditrec1"].submit();
                                        }   
                                        function printreceives(obj){
                                            var rec_id = obj.getAttribute("rec_id");
                                            console.log(rec_id);

                                            document.forms["formeditrec1"].action="receipt.php?formedit="+rec_id;
                                            document.forms["formeditrec1"].target ="_blank"
                                            document.forms["formeditrec1"].submit();
                                        }

                                        function open_list_rec(obj){
                                           
                                            document.forms["formeditrec1"].action="assess1.php";
                                            document.forms["formeditrec1"].submit();
                                        } 
                                    </script>
                                    <!-- EDIT -->
							</tbody>
						</table>
					
				</div> <!-- /widget-content -->
			
			</div> <!-- /widget -->
        </div>
    </div>
</div>
    </body>
</head>
<?php
    include "setting/config.php";
?>
<?php
    @session_start();
    @session_cache_expire(30);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ข้อมูลส่วนตัว</title>

    <style>
        @font-face {
            font-family: 'KRR_AengAei.ttf';
            src: url('fonts/KRR_AengAei.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;     
        }
        
        .card{
            background-color: #ffffff;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            text-align: center;
        }
        .bg{
            color:#000000  ;
        }

        .navbar{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
   /* height:100vh;*/
    color: #fff;


    width:100%; 
    height:90px;

}




.fo{ 
    background: #4143A3;
    /* For browsers that do not support gradients */
    background: -webkit-linear-gradient(left top, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(bottom right, #1E1E45, #2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* For Firefox 3.6 to 15 */
    background: linear-gradient( to bottom right, #1E1E45,#2554C7, #306EFF,#3BB9FF,#5CB3FF);
    /* Standard syntax */
    height:30vh;
    color: #fff;
}

.form-group{
    color:#000000 ;
}


.sidenav {
  height: 72%;
  width: 15%;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #DBDDE7;
  overflow-x: hidden;
  padding-top: 20px;
}

.sidenav a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
}

.sidenav a:hover {
  color: #f1f1f1;
}

    </style>

</head>
<body>
    
<script>
function acceptupdate(){
document.forms["formupdateuser"].action="updateowners.php";
document.forms["formupdateuser"].submit();

  }

function edituser(obj){
 
    var ownname =obj.getAttribute("uname");
    var owntel =obj.getAttribute("utel");
    var ownadd =obj.getAttribute("uadd");
    var ownemail =obj.getAttribute("uemail");
    var ownpass =obj.getAttribute("upass");
    var ownstatus =obj.getAttribute("ustatus");
    var ownservice =obj.getAttribute("uservice");

    document.getElementById("uname").value = ownname;
    document.getElementById("utel").value = owntel;
    document.getElementById("utelold").value = owntel;
    document.getElementById("uadd").value = ownadd;
    document.getElementById("uemail").value = ownemail;
    document.getElementById("upass").value = ownpass;
    document.getElementById("ustatus").value = ownstatus;
    document.getElementById("uservice").value = ownservice;

}
/*<!-- ลบข้อมูล -->*/

var globalid;
function sdeletedata(obj){
   
    var protid =obj.getAttribute("delete_id");
    var protname =obj.getAttribute("delete_name");
    globalid=protid

    document.getElementById("deletename").innerHTML = protname;
    document.getElementById("deleteid").innerHTML = protid;
}

function deletedata(){
  //  alert(globalid)
//alert(globalid);
    $.ajax({ //กล่องๆนึงรวมข้อมูลไว้
                type:"POST",
                async :false, //ทำให้ทำงานเรียงตามลำดับ
                url:"webservice/deleteowner.php",
                data:{
                    delete_id:globalid
                }
            });
            location.reload();
}
</script>
<div class="modal" id="edit" >
    <div class="modal-dialog modal-lg" style="border:4px #FF6D33  solid;border-radius :1%;box-shadow: 10px 10px 5px #4B4B4B ;">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header d-block">
          <h2 class="modal-title font" style="font-weight: bold;" align="center">แก้ไขข้อมูลผู้ใช้งาน</h2>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body font">
        <center><img src="image/edit-valid.png" wigth="200px" height="200px" ></center>
        <br>
        
        <?php
          $strSQL="SELECT * FROM owners WHERE ow_status = 'เจ้าของกิจการ' AND ow_name != '".$_SESSION['ow_name']."' ORDER BY ow_id ASC" ;
          $result=@$conn->query($strSQL);
        ?>

        <form id="formupdateuser" name="formupdateuser" method="POST">
                                      <label class="font" style="font-size: 20px;">ชื่อ:</label>
                                        <input type="text" class="form-control" id="uname" name="uname" placeholder="ชื่อ">
                                          <br>
                                        <label class="font" style="font-size: 20px;">เบอร์โทรศัพท์:</label>
                                        <input  type="text" class="form-control" id="utel" name="utel" placeholder="เบอร์โทรศัพท์" maxlength="10" minlength="10">
                                        <input   type="text" class="form-control" id="utelold" name="utelold" hidden maxlength="10" minlength="10">
                                        <br>
                                        <label class="font" style="font-size: 20px;">สถานะ:</label>
                                        <select class="form-control"  id="ustatus" name="ustatus"  >
                                        <option value="เจ้าของกิจการ">เจ้าของกิจการ</option>
                                        <option value="ลูกค้า">ลูกค้า</option>
                                        </select>                                        <br>
                                        <label class="font" style="font-size: 20px;">ที่อยู่:</label>
                                        <textarea type="text" class="form-control" id="uadd" name="uadd" placeholder="ที่อยู่"></textarea>
                                        <br>
                                        <label class="font" style="font-size: 20px;">อีเมล:</label>
                                        <input type="text" class="form-control" id="uemail" name="uemail" placeholder="อีเมล"></input>
                                        <br>
                                        <label class="font" style="font-size: 20px;">รหัสผ่าน:</label>
                                        <input type="password" class="form-control" id="upass" name="upass" placeholder="รหัสผ่าน"></input>
                                        <br>
                                     
                                      <!-- Modal footer -->
        <div class="modal-footer d-block" align="center"  style="background-color: #E2FA6E;">
          <button id="mdcolor" class="btn btn-success" type="button" data-dismiss="modal" onclick="acceptupdate()">ยืนยันการแก้ไข</button>
          <button id="mdcolor" class="btn btn-danger" type="button" data-dismiss="modal">ยกเลิก</button>
        </div>
      </div>
    </div>
  </div>

                    </table>            
        </div>
</body>
</html>